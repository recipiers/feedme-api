# FEEDME-API

## Como hacer requisiciones ?
Requisiciones que envien la informacion en formato json pueden ser hechas ( y son recomendadas ) si se añade el Header apropriado
```
Content-Type: application/json
```
## Endpoints

### Realizar una búsqueda de recetas

Obtiene todas las recetas que satisfagan los criterios de búsqueda dados.

Si hay una sesión de usuario activa, además, se incluirá también información de si esas recetas están entre sus favoritas o no.

Tener en cuenta que esto no da información concreta sobre los ingredientes, los pasos, los alérgenos, etc. de las recetas obtenidas. Para obtener esa información se debe buscar la receta concreta de forma individual. 

- Método: `POST`

```
/api/recipes/search
```

#### Parámetros Body

Todos los parámetros siguientes son opcionales. Omitirlos todos da los mismos resultados que el endpoint de obtener todas las recetas.

- `name`: Los resultados de recetas se reducen a aquellos que poseen en su nombre el valor de este parámetro.

- `includedIngredients`: Lista de ingredientes que incluirán las recetas buscadas. Contiene los IDs de los ingredientes en cuestión.

- `excludedIngredients`: Lista de ingredientes que no incluirán las recetas buscadas. Contiene los IDs se los ingredientes en cuestión.

- `isvegetaria`: Si vale `true`, solamente incluirá en los resultados recetas vegetarianas o veganas. Si vale `false`, este parámetro es ignorado. Si `isvegan = true`, se ignora el valor de este parámetro.

- `isvegan`: Si vale `true`, solamente incluirá en los resultados recetas veganas. Si vale `false`, este parámetro es ignorado. Cuando es `true`, tiene prioridad sobre el valor de `isvegetaria`

- `maxtime`: Tiempo máximo de elaboración del cual se incluirán recetas en los resultados. Si vale `0` o se omite, se asume que no hay tiempo de elaboración máximo especificado y se ignora.

- `allergens`: Se excluyen todas las recetas que contengan alguno de los alérgenos indicados. Esto es, si al menos uno de los ingredientes de la receta contiene al menos uno de los alérgenos especificados, se descartará dicha receta. Contiene el nombre de los alérgenos en cuestión ejemplo "gluten" . Si se omite, no se descartarán recetas por alérgenos.

- `tags`: Solamente incluye en los resultados de la búsqueda aquellas recetas que contengan las tags especificadas.

#### Ejemplo:

##### Petición: Parámetros BODY:

```json
{
    "includedIngredients": [1, 2, 3],
    "excludedIngredients": [4],
    "isvegetaria": true,
    "isvegan": false,
    "maxtime": 25,
    "allergens": ["gluten"],
    "tags": ["salsa"]
}
```

##### Respuesta

```json

{
    "success": true,
    "status": 200,
    "search": {
        "includedIngredients": [1, 2, 3],
        "excludedIngredients": [4],
        "isvegetaria": true,
        "isvegan": false,
        "maxtime": 25,
        "allergens": ["gluten"],
        "tags": ["salsa"]
    },
    "recipes": [
        {
            "id": 25,
            "name": "Salsa pomodoro",
            "description": "Salsa de tomate de preparación rápida, utilizable para complementar pizzas y pasta",
            "time": 25,
            "pictureurl": "https://assets.bonappetit.com/photos/57ae32601b33404414975b70/16:9/w_2560,c_limit/quick-pomodoro-sauce-646.jpg",
            "numportions": 2,
            "difficulty": 2
        },
        {
        
            "id": 13,
            "name": "Salsa carbonara (versión vegetariana)",
            "description": "Receta de salsa carbonara apta para vegetarianos",
            "time": 20,
            "pictureurl": "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTcqk8X6JEmKr60RgmsghoOdeZ8YI5I84Felx5oBdUNCfWE1FaK&usqp=CAU",
            "numportions": 4,
            "difficulty": 2
        },
        [...]
    ]
}
```

### Realizar búsqueda almacenada de recetas

Obtiene la información relativa a una búsqueda almacenada por un usuario y la ejecuta.

Funciona de manera similar que el endpoint anterior, pero los parámetros los toma del objeto búsqueda especificado.

- Método: `GET`
- Requiere: Autenticación

```
/api/search/${id}/perform
```

#### Parámetros GET

- id: Identificador de la búsqueda almacenada.

#### Ejemplo

### Obtener todos los ingredientes

Obtiene todos los ingredientes almacenados en la base de datos.

Método: `GET`

```
/api/ingredients
```

#### Ejemplo

##### Petición: URL

```
GET /api/ingredients
```

##### Respuesta

```json
{
    "status": 200,
    "message": "OK",
    "ingredients": [
        {
            "id": 1,
            "name": "cebolla",
            "description": "Cebolla natural",
            "isvegetaria": true,
            "isvegan": true,

        },
        {
            "id": 2,
            "name": "ajo",
            "description": "Ajo natural",
            "isvegetaria": true,
            "isvegan": true,

        },
        {
            "id": 3,
            "name": "pimienta negra molida",
            "description": "Pimienta negra molida natural",
            "isvegetaria": true,
            "isvegan": true
        },
        [...]
    ] 
}

```

### Obtener todos los alérgenos

Obtiene todos los alérgenos almacenados en la base de datos.

Método: `GET`

```
/api/allergens
```

#### Ejemplo

##### Petición: URL

```
GET /api/allergens
```

##### Respuesta

```json
{
    "status": 200,
    "message": "OK",
    "allergens": [
         {
            "id": 1,
            "name": "lácteos",
            "description": "Este elemento contiene lactosa, con lo cual no está indicado para personas intolerantes a la lactosa."
        },
        {
            "id": 2,
            "name": "gluten",
            "description": "Este elemento contiene gluten, con lo cual no está indicado para personas celíacas."
        },
        {
            "id": 3,
            "name": "frutos secos",
            "description": "Este elemento contiene frutos secos, con lo cual no está indicado para personas intolerantes o alérgicas a los frutos secos."
        },
        [...]
    ] 
}

```
### Obtener parametros de búsqueda primaria del usuario logueado

Obtiene los parámetros de búsqueda especificados en la búsqueda primaria del usuario logueado en la API.

Todos los usuarios tienen una búsqueda por defecto, que especifica los valores predeterminados para la realización de cualquier búsqueda.

El usuario del cual se obtienen las búsquedas es el usuario que esté logueado a la API mediante el header `Authorization`.

- Método: `GET`
- Requiere Autenticación

```
/api/user/primarySearch/
```

#### Ejemplo

##### Petición: URL

```
GET /api/user/primarySearch
```

##### Respuesta

```json
{
    "status": 200,
    "message": "OK",
    "search" : {
        "includedIngredients": [],
        "excludedIngredients": [],
        "isvegetaria": false,
        "isvegan": false,
        "maxtime": 60,
        "allergens": ["gluten"],
        "tags": []   
    },
}

```

#### ERRORES:

##### No autorizado

Si no existe una sesión de usuario activa en el momento de enviar una petición a este endpoint, se obtendrá un error `401` (`Unauthorized`).

```json
{
    "success": false,
    "status": 401,
    "message": "Se requiere una sesión de usuario activa para realizar esta operación" 
}
```

### Obtener búsquedas guardadas del usuario logueado

Obtiene la lista de búsquedas guardadas por el usuario de los parámetros de búsqueda especificados en la búsqueda por defecto del usuario.

Todos los usuarios tienen una búsqueda por defecto, que especifica los valores predeterminados para la realización de cualquier búsqueda.

El usuario del cual se obtienen las búsquedas es el usuario que esté logueado a la API mediante el header `Authorization`.

Las búsquedas almacenadas tendrán un título humanamente legible para que el usuario las pueda identificar y diferenciar del resto.

```
/api/user/storedSearches
```

#### EJEMPLO

##### Petición URL

```
GET /api/user/storedSearches
```

##### Respuesta

```json
{
    "status": 200,
    "success": true,
    "message": "OK",
    "storedSearches": [
        {
            "id": 17,
            "title": "Salsas vegetarianas con tomate",
            "name": "tomate",
            "includedIngredients": [7],
            "excludedIngredients": [],
            "isvegetaria": true,
            "isvegan": false,
            "maxtime": 0,
            "allergens": [],
            "tags": ["salsa"] 
        },
        {
            "id": 25,
            "title": "Ensaladas rápidas de hacer sin cebolla",
            "name": "ensalada",
            "includedIngredients": [],
            "excludedIngredients": [1],
            "isvegetaria": false,
            "isvegan": false,
            "maxtime": 20,
            "allergens": [],
            "tags": ["ensalada"]
        },
        {
            "id": 32,
            "title": "Comida para perros",
            "includedIngredients": [],
            "excludedIngredients": [],
            "isvegetaria": false,
            "isvegan": false,
            "maxtime": 0,
            "allergens": [],
            "tags": ["comida-de-perro"]
        }
    ]
}
```

### Actualizar búsqueda primaria del usuario logueado

Modifica los criterios de búsqueda de la búsqueda por defecto, es decir, los valores por defecto que se establecerán al realizar una búsqueda, del usuario logueado actualmente en la API.

La búsqueda primaria no necesita título.

- Método: `PUT`
- Requiere: Autenticación

```
/api/user/primarySearch
```

#### Parámetros BODY

Todos los parámetros son opcionales. La omisión de todos los parámetros dará como resultado una búsqueda sin criterios, que devolverá todas las recetas.

- `name`: parámetro de búsqueda por nombre. Todas las recetas dadas por la búsqueda contendrán en el nombre el valor de este atributo.

- `includedIngredients`: Lista de ingredientes que incluirán las recetas buscadas. Contiene los IDs de los ingredientes en cuestión.

- `excludedIngredients`: Lista de ingredientes que no incluirán las recetas buscadas. Contiene los IDs se los ingredientes en cuestión.

- `isvegetaria`: Si vale `true`, solamente incluirá en los resultados recetas vegetarianas o veganas. Si vale `false`, este parámetro es ignorado. Si `isvegan = true`, se ignora el valor de este parámetro.

- `isvegan`: Si vale `true`, solamente incluirá en los resultados recetas veganas. Si vale `false`, este parámetro es ignorado. Cuando es `true`, tiene prioridad sobre el valor de `isvegetaria`

- `maxtime`: Tiempo máximo de elaboración del cual se incluirán recetas en los resultados. Si vale `0` o se omite, se asume que no hay tiempo de elaboración máximo especificado y se ignora.

- `allergens`: Se excluyen todas las recetas que contengan alguno de los alérgenos indicados. Esto es, si al menos uno de los ingredientes de la receta contiene al menos uno de los alérgenos especificados, se descartará dicha receta. Contiene los IDs de los alérgenos en cuestión. Si se omite, no se descartarán recetas por alérgenos.

- `tags`: Solamente incluye en los resultados de la búsqueda aquellas recetas que contengan las tags especificadas.

#### EJEMPLO

##### Petición URL

```
PUT /api/user/primarySearch
```

##### Parámetros BODY

```json
{
    "includedIngredients": [],
    "excludedIngredients": [],
    "isvegetaria": false,
    "isvegan": true,
    "maxtime": 75,
    "allergens": [],
    "tags": []
}
```

##### Respuesta

```json
{
    "status": 200,
    "success": true,
    "message": "Búsqueda primaria actualizada con éxito",
    "oldPrimarySearch": {
        "includedIngredients": [],
        "excludedIngredients": [],
        "isvegetaria": true,
        "isvegan": false,
        "maxtime": 50,
        "allergens": [],
        "tags": []
    },
    "newPrimarySearch": {
        "includedIngredients": [],
        "excludedIngredients": [],
        "isvegetaria": false,
        "isvegan": true,
        "maxtime": 75,
        "allergens": [],
        "tags": []   
    }
}
```

#### ERRORES

##### No autorizado

Puesto que la búsqueda es primaria con relación a un usuario activo, es necesario que exista una sesión de usuario de la API. 

En caso contrario, se devolverá una respuesta de error con este formato:

```json
{
    "success": false,
    "status": 401,
    "message": "Se requiere una sesión de usuario activa para realizar esta operación"
}
```

### Actualizar búsqueda guardada del usuario logueado

Modifica los criterios de búsqueda de una de las búsquedas almacenadas de usuario logueado actualmente en la API.

Método: `PUT`

```
/api/user/storedSearches/${id}
```

#### Parámetros GET

- `id`: El identificador de la búsqueda guardada a editar. Tener en cuenta que la búsqueda debe corresponder al usuario que realiza la petición.

#### Parámetros POST

Todos los parámetros son opcionales.

- `title`: El título con el que el usuario identifica la búsqueda almacenada. Se recomienda que un usuario no tenga almacenadas dos búsquedas con el mismo título.

- `name`: parámetro de búsqueda por nombre. Todas las recetas dadas por la búsqueda contendrán en el nombre el valor de este atributo.

- `includedIngredients`: Lista de ingredientes que incluirán las recetas buscadas. Contiene los IDs de los ingredientes en cuestión.

- `excludedIngredients`: Lista de ingredientes que no incluirán las recetas buscadas. Contiene los IDs se los ingredientes en cuestión.

- `isvegetaria`: Si vale `true`, solamente incluirá en los resultados recetas vegetarianas o veganas. Si vale `false`, este parámetro es ignorado. Si `isvegan = true`, se ignora el valor de este parámetro.

- `isvegan`: Si vale `true`, solamente incluirá en los resultados recetas veganas. Si vale `false`, este parámetro es ignorado. Cuando es `true`, tiene prioridad sobre el valor de `isvegetaria`

- `maxtime`: Tiempo máximo de elaboración del cual se incluirán recetas en los resultados. Si vale `0` o se omite, se asume que no hay tiempo de elaboración máximo especificado y se ignora.

- `allergens`: Se excluyen todas las recetas que contengan alguno de los alérgenos indicados. Esto es, si al menos uno de los ingredientes de la receta contiene al menos uno de los alérgenos especificados, se descartará dicha receta. Contiene los IDs de los alérgenos en cuestión. Si se omite, no se descartarán recetas por alérgenos.

- `tags`: Solamente incluye en los resultados de la búsqueda aquellas recetas que contengan las tags especificadas.

#### Ejemplo

#### Petición: URL

```
PUT /api/user/storedSearches/137
```

#### Parámetros BODY

```json
{
    "title": "Desayunos",
    "includedIngredients": [],
    "excludedIngredients": [],
    "isvegetaria": false,
    "isvegan": false,
    "maxtime": 20,
    "allergens": [],
    "tags": ["desayuno"]
}
```

#### Respuesta

```json
{
    "success": true,
    "status": 200,
    "message": "Búsqueda almacenada modificada con éxito",
    "oldSearch": {
        "title": "Mañana",
        "includedIngredients": [1, 3],
        "excludedIngredients": [2],
        "isvegetaria": false,
        "isvegan": false,
        "maxtime": 30,
        "allergens": [],
        "tags": []
    },
    "newSearch": {
        "title": "Desayunos",
        "includedIngredients": [],
        "excludedIngredients": [],
        "isvegetaria": false,
        "isvegan": false,
        "maxtime": 20,
        "allergens": [],
        "tags": ["desayuno"]
    },
    
}

```

#### Errores

##### No autorizado

Con tal de acceder a las búsquedas del usuario logueado, se requiere que haya un usuario de la API conectado vía token de usuario válida como valor del header `Authorization`.

De no ser así, se devolverá una respuesta de error con este formato:

```json
{
    "success": false,
    "status": 401,
    "message": "Se requiere una sesión de usuario activa para realizar esta operación"
}
```

##### Búsqueda no encontrada

Si la búsqueda correspondiente al ID enviado no existe, se devolverá una respuesta de error con este formato:

```json
{
    "success": false,
    "status": 404,
    "message": "No hay ninguna búsqueda almacenada asociada a ese ID."
}
```

### Guardar nueva búsqueda personalizada de usuario

Crea una nueva búsqueda almacenada de usuario.

Un usuario podrá almacenar un máximo de 3 búsquedas personalizadas.

- Método: `POST`
- Requiere: Autenticación

```
/api/user/storedSearches
```

#### Parámetros POST

Todos los parámetros son opcionales.

- `title`: El título con el que el usuario identifica la búsqueda almacenada. Se recomienda que un usuario no tenga almacenadas dos búsquedas con el mismo título.

- `name`: parámetro de búsqueda por nombre. Todas las recetas dadas por la búsqueda contendrán en el nombre el valor de este atributo.

- `includedIngredients`: Lista de ingredientes que incluirán las recetas buscadas. Contiene los IDs de los ingredientes en cuestión.

- `excludedIngredients`: Lista de ingredientes que no incluirán las recetas buscadas. Contiene los IDs se los ingredientes en cuestión.

- `isvegetaria`: Si vale `true`, solamente incluirá en los resultados recetas vegetarianas o veganas. Si vale `false`, este parámetro es ignorado. Si `isvegan = true`, se ignora el valor de este parámetro.

- `isvegan`: Si vale `true`, solamente incluirá en los resultados recetas veganas. Si vale `false`, este parámetro es ignorado. Cuando es `true`, tiene prioridad sobre el valor de `isvegetaria`

- `maxtime`: Tiempo máximo de elaboración del cual se incluirán recetas en los resultados. Si vale `0` o se omite, se asume que no hay tiempo de elaboración máximo especificado y se ignora.

- `allergens`: Se excluyen todas las recetas que contengan alguno de los alérgenos indicados. Esto es, si al menos uno de los ingredientes de la receta contiene al menos uno de los alérgenos especificados, se descartará dicha receta. Contiene los IDs de los alérgenos en cuestión. Si se omite, no se descartarán recetas por alérgenos.

- `tags`: Solamente incluye en los resultados de la búsqueda aquellas recetas que contengan las tags especificadas.

#### Ejemplo

##### Petición URL

```
POST /api/user/storedSearches
```

##### Parámetros BODY

```json
{
    "title": "Desayunos",
    "includedIngredients": [],
    "excludedIngredients": [],
    "isvegetaria": false,
    "isvegan": true,
    "maxtime": 20,
    "allergens": [],
    "tags": ["desayuno"]
}
```

##### Respuesta

``````json
{
    "success": true,
    "status": 200,
    "message": "Búsqueda almacenada insertada con éxito",
    "search": {
        "title": "Desayunos",
        "includedIngredients": [],
        "excludedIngredients": [],
        "isvegetaria": false,
        "isvegan": false,
        "maxtime": 20,
        "allergens": [],
        "tags": ["desayuno"]
    }
}
``````

#### Errores

##### No autorizado

Si no existe una sesión de usuario activa en el momento de enviar una petición a este endpoint, se obtendrá un error `401` (`Unauthorized`).

```json
{
    "success": false,
    "status": 401,
    "message": "Se requiere una sesión de usuario activa para realizar esta operación" 
}
```

##### Máximo de búsquedas guardadas alcanzado

Un usuario tiene un límite de almacenamiento de 3 búsquedas. 

Si se intenta almacenar otra búsqueda habiéndose alcanzado ese límite, se enviará una respuesta de error con el siguiente formato:

```json
{
    "success": false,
    "status": 200,
    "message": "Un usuario no puede tener más de tres búsquedas almacenadas."
}
```

### Borrar búsqueda personalizada de usuario

Elimina una de las búsquedas de usuario almacenadas.

La búsqueda a borrar debe pertenecer al usuario que la elimina.

Método: `DELETE`
Requiere: Autenticación

```
/api/user/storedSearches/${id}
```

#### Parámetros GET

- id: ID de la búsqueda a eliminar.

#### Ejemplo

##### Petición URL

```
DELETE /api/user/storedSearches/32
```

##### Respuesta

```json
{
    "success": true,
    "message": "Búsqueda borrada con éxito",
    "search": {
        "id": 32,
        "title": "Comida para perros",
        "includedIngredients": [],
        "excludedIngredients": [],
        "isvegetaria": false,
        "isvegan": false,
        "maxtime": 0,
        "allergens": [],
        "tags": ["comida-de-perro"]
    }
}
```
#### Errores

##### No autorizado

Si no existe una sesión de usuario activa en el momento de enviar una petición a este endpoint, se obtendrá un error `401` (`Unauthorized`).

```json
{
    "success": false,
    "status": 401,
    "message": "Se requiere una sesión de usuario activa para realizar esta operación" 
}
```

##### Búsqueda no encontrada

En caso de que no exista ninguna búsqueda almacenada con el ID indicado, se devolverá una respuesta de error con este formato:

```json
{
    "success": false,
    "status": 404,
    "message": "No se ha encontrado la búsqueda con el ID especificado" 
}
```

### Ver receta 

Obtiene la información detallada relativa a la receta indicada. 

También obtiene su lista de ingredientes con cantidades, sus alérgenos, etiquetas y si se encuentra entre las recetas favoritas del usuario en caso de haber un usuario logueado en la aplicación.

Método: `GET`

```
/api/recipes/${id}
```

#### Parámetros GET:

- `id`: Id de la receta buscada.

#### Ejemplo

##### Petición URL

```
GET /api/recipes/13
```

##### Respuesta

```json
{
    "status": 200,
    "success": true,
    "message": "OK",
    "recipe": {
        "id": 1,
        "name": "Tortilla francesa",
        "description": "Receta sencilla de tortilla francesa",
        "time": 5,
        "pictureurl": "https:\/\/vdmedia.elpais.com\/elpaistop\/201711\/8\/201711081227871_1510141189_asset_still.png",
        "numportions": 1,
        "difficulty": 1,
        "steps": [
            "Poner una sartén en el fuego y echarle un chorrito de aceite",
            "Una vez el aceite se ha calentado, echarle un huevo.",
            "Remover los huevos y esperar a que cuajen. Se puede dejar más o menos tiempo, en función del nivel de cocción deseado."
        ],
        "ingredients": [
            {
                "ingredient": {
                    "id": 2,
                    "name": "huevos",
                    "description": "Huevos de gallina, tamaño mediano",
                    "isvegan": 0,
                    "isvegetaria": 1
                },
                "amount": 1.000,
                "unit": {
                    "id": 14,
                    "name": "unidad(es)",
                    "symbol": "u.",
                    "measure_unit_type_id": 3,
                    "genericequivalent": "1.000"
                }
                }
            ],
            "tags": []
        } 
    }
}
```

#### ERRORES

##### Receta no encontrada

En caso de no existir ninguna receta con el ID especificado, se devolverá una respuesta de error con el siguiente formato:

```json
{
    "success": false,
    "status": 404,
    "message": "No se ha encontrado la receta con el ID especificado" 
}
```

### Marcar/desmarcar receta como favorita

Marca una receta como favorita del usuario.

En caso de que la receta ya esté marcada como favorita, dejará de estarlo.

- Método: `POST`
- Requiere: Autenticación

```
/api/user/favrecipes/${id}
```

#### Parámetros GET

- `id`: Identificador de la receta que se desea marcar/desmarcar de favoritos para el usuario actual.

#### Ejemplo

##### Petición URL

```
POST /api/user/favrecipes/13
```

##### Respuesta

```json
{
    "status": 200,
    "success": true,
    "message": "Receta marcada como favorita",
    "recipe": {
        "id": 13,
        "name": "Fresas con nata",
        "time": 5,
        "pictureurl": null,
        "numportions": 1,
        "difficulty": 1,
        "isuserfav": true
    }
}
```

Y si realiza la misma petición de nuevo...


```json
{
    "status": 200,
    "success": true,
    "message": "Receta desmarcada de favoritas",
    "recipe": {
        "id": 13,
        "name": "Fresas con nata",
        "time": 5,
        "pictureurl": null,
        "numportions": 1,
        "difficulty": 1,
        "isuserfav": false
    }
}
```

#### Errores

##### No autorizado

Si no existe una sesión de usuario activa en el momento de enviar una petición a este endpoint, se obtendrá un error `401` (`Unauthorized`).

```json
{
    "success": false,
    "status": 401,
    "message": "Se requiere una sesión de usuario activa para realizar esta operación" 
}
```

---
### Obtener recetas favoritas del usuario logueado

Obtiene la lista de recetas favoritas del usuario.

El usuario del cual se obtienen las recetas favoritas es el usuario que esté logueado a la API mediante el header `Authorization`.

- Método: `GET`

```
/api/user/favrecipes
```

#### EJEMPLO

##### Petición URL

```
GET /api/user/favrecipes
```

##### Respuesta

```json
{
    "status": 200,
    "success": true,
    "message": "OK",
    "recipes": [
        {
        
            "id": 14,
            "name": "Paella valenciana",
            "description": "Paella valenciana de la de verdad, mixta de toda la vida.",
            "time": 200,
            "pictureurl": "https://www.comedera.com/wp-content/uploads/2014/02/paella-de-mariscos-700x400.jpg",
            "numportions": 6,
            "difficulty": 5
    ]
}
```



---




### Iniciar sesión de usuario

Inicia una sesión de usuario.

La sesión de usuario se maneja mediante JWT (JSON web token).

Dicha token se debe incluir como valor del header `Authorization` con tal de que se haga efectiva la sesión.

La token se genera con un tiempo de vida variable, el cual transcurrido, inutiliza la token y crea la necesidad de renovarla.

- Método: `POST`

```
/api/login
```

#### Parámetros BODY

- `email`: El email del usuario se utiliza como identificador de autenticación.
- `password`: La contraseña introducida debe corresponderse con la del usuario que inicia sesión.

#### Ejemplo

##### Parámetros BODY

```json
{
    "email": "david@feedme.com",
    "password": "123456789"
}
```

##### Respuesta

En caso de login correcto, se recibirá una respuesta con un usuario y la token, con el mismo formato que la que sigue: 

```json
{
    "success": true,
    "message": "Login OK",
    "status": 200,
    "user": {
        "sub": 15,
        "email": "david@feedme.com",
        "name": "david",
        "iat": 1591627490,
        "exp": 1591713890
    },
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjE1LCJlbWFpbCI6ImRhdmlkQHBydWViYS5jb20iLCJuYW1lIjoiZGF2aWQiLCJpYXQiOjE1OTE2Mjc0OTAsImV4cCI6MTU5MTcxMzg5MH0.GmgkASNCzVvzLfdifO2U1-QkFfzF12Of4bINRTPL_-0"
}
```

> **NOTA**: La respuesta anterior es un ejemplo y la token obtenida puede no corresponderse con los datos de la petición. 

#### Errores

##### Credenciales incorrectas

En caso de que el usuario introducido no exista o bien la contraseña no coincida, se producirá un error.

La respuesta dada será una respuesta de error con el siguiente formato:

```json
{
  "success": false,
  "message": "Fallo de autenticación",
  "status": 401,
  "user": null,
  "token": null
}
```


### Registrar usuario

Registra un nuevo usuario.

- Método: `POST`

```
/api/register
```

#### Parámetros BODY

- `email`: El email del usuario se utiliza como identificador de autenticación.
- `name`: Nombre de usuario. 
- `password`: La contraseña introducida debe corresponderse con la del usuario que inicia sesión.
- `password_confirmation`: Debe coincidir con el valor de `password`.

#### Ejemplo

##### Parámetros BODY

```json
{
    "email": "atilla@feedme.com",
    "name": "Aitor Tilla",
    "password": "123456789",
    "password_confirmation": "123456789"
}
```

##### Respuesta

En caso de registro correcto, se recibirá una respuesta con un usuario y la token, con el mismo formato que la que sigue: 

```json
{
    "success": true,
    "message": "Registro OK",
    "status": 200,
    "user": {
        "sub": 15,
        "email": "atilla@feedme.com",
        "name": "Aitor Tilla",
        "iat": 1591627490,
        "exp": 1591713890
    },
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjE1LCJlbWFpbCI6ImRhdmlkQHBydWViYS5jb20iLCJuYW1lIjoiZGF2aWQiLCJpYXQiOjE1OTE2Mjc0OTAsImV4cCI6MTU5MTcxMzg5MH0.GmgkASNCzVvzLfdifO2U1-QkFfzF12Of4bINRTPL_-0"
}
```

Esto da la opción a la aplicación de usar la token para establecer la sesión.

> **NOTA**: La respuesta anterior es un ejemplo y la token obtenida puede no corresponderse con los datos de la petición. 


### Cerrar sesión de usuario

Elimina la sesión de usuario actual.

- Método: `POST`
- Requiere: Autenticación

```
/api/logout
```

#### Ejemplo

##### Petición URL

```
POST /api/logout
```

##### Respuesta

```json
{
    "status": 200,
    "message": "Sesión cerrada con éxito"
}
```

#### Errores

##### No autorizado

Si no existe una sesión de usuario activa en el momento de enviar una petición a este endpoint, se obtendrá un error `401` (`Unauthorized`).

```json
{
    "success": false,
    "status": 401,
    "message": "Se requiere una sesión de usuario activa para realizar esta operación" 
}
```

### Ver perfil de usuario actual

Obtiene los datos del usuario logueado actualmente en la aplicación.

Incluye recetas favoritas, búsquedas almacenadas, búsqueda primaria.

- Método: `GET`
- Requiere: Autenticación

```
/api/user/me
```

#### Ejemplo

##### Petición URL

```
GET /api/user/me
```

##### Respuesta

```json
{
  "success": true,
  "status": 200,
  "message": "OK",
  "user": {
    "id": 15,
    "name": "david",
    "isAdmin": 0,
    "email": "david@prueba.com",
  }
}
```

#### Errores

##### No autorizado

Si no existe una sesión de usuario activa en el momento de enviar una petición a este endpoint, se obtendrá un error `401` (`Unauthorized`).

```json
{
    "success": false,
    "status": 401,
    "message": "Se requiere una sesión de usuario activa para realizar esta operación" 
}
```

### Cambiar contraseña de usuario actual

Modifica el valor de la contraseña de usuario actual

- Método: `POST`
- Requiere: Autenticación

```
/api/user/changepass
```

#### Parámetros POST:

- oldpassword: Contraseña antigua.
- newpassword: Contraseña nueva.
- newpassword_confirmation: Debe coincidir con la contraseña nueva.

### Eliminar cuenta de usuario

Elimina la cuenta del usuario de la sesión actual.

Es irreversible y eliminará también todas las asociaciones de recetas favoritas y de búsquedas almacenadas del usuario. 

Se recomienda hacer consciente al usuario de la aplicación de toda esta información antes de que acceda a realizar esta operación.

- Método: `DELETE`
- Requiere: Autenticación

```
/api/user
```

#### Ejemplo

##### Petición URL

```
DELETE /api/user
```

##### Respuesta

```json
{
    "status": 200,
    "success": true,
    "message": "Cuenta de usuario eliminada con éxito"
}
```

#### Errores

##### No autorizado

Puesto que esta acción se realiza sobre el mismo usuario de la sesión, es necesario que exista una sesión de usuario activa para realizar la operación.

En caso de que dicha sesión no esté activa, se devolverá un error con la siguiente estructura:

```json
{
    "success": false,
    "status": 401,
    "message": "Se requiere una sesión de usuario para realizar esta acción"
}
```
