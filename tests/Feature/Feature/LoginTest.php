<?php

namespace Tests\Feature\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LoginTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testRequiresEmailPassowrd()
    {
        $this->json('POST,''api/login')
        ->assertStatus(422)
        ->assertJson([
            'email' => ['The email field is required.'],
            'passowrd' => ['The password field is required']
        ]);
    }
}
