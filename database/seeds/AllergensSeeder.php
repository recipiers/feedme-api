<?php

use Illuminate\Database\Seeder;

use App\Model\Allergen;

class AllergensSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Allergen::create([
            'name' => 'lácteos',
            'description' => 'Este elemento contiene lactosa, con lo cual no está indicado para personas intolerantes a la lactosa.'
        ]);

        Allergen::create([
            'name' => 'gluten',
            'description' => 'Este elemento contiene gluten, con lo cual no está indicado para personas celíacas.'
        ]);

        Allergen::create([
            'name' => 'frutos secos',
            'description' => 'Este elemento contiene frutos secos, con lo cual no está indicado para personas intolerantes o alérgicas a los frutos secos.'
        ]);
    }
}
