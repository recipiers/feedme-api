<?php

use App\Model\Allergen;
use Faker\Factory;
use Illuminate\Database\Seeder;

use App\Model\Ingredient;

class IngredientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        // Ingredient::truncate();
        // alergias
        // lácteos, gluten, frutos secos
        $lacteo = Allergen::where('name','=','lácteos')->pluck('id')->toArray()[0];
        $gluten = Allergen::where('name','=','gluten')->pluck('id')->toArray()[0];
        $frutoseco = Allergen::where('name','=','frutos secos')->pluck('id')->toArray()[0];

        $ingr001 = Ingredient::create([
            'name' => 'leche',
            'description' => 'leche de vaca',
            'isvegan' => false,
            'isvegetarian' => true
        ])->allergens()->attach([
            'allergen_id' => $lacteo
        ]);

        // $ingr001->allergens()->create();

        Ingredient::create([
            'name' => 'huevos',
            'description' => 'Huevos de gallina, tamaño mediano',
            'isvegan' => false,
            'isvegetarian' => true
        ]);

        Ingredient::create([
            'name' => 'harina de trigo',
            'description' => 'harina de trigo CONTIENE GLUTEN',
            'isvegan' => true,
            'isvegetarian' => true
        ])->allergens()->attach([
            'allergen_id' => $gluten
        ]);

        Ingredient::create([
            'name' => 'chocolate con leche',
            'description' => 'Chocolate oscuro con leche',
            'isvegan' => false,
            'isvegetarian' => true
        ])->allergens()->attach([
            'allergen_id' => $lacteo
        ]);

        Ingredient::create([
            'name' => 'chocolate negro',
            'description' => 'Chocolate negro',
            'isvegan' => true,
            'isvegetarian' => true
        ]);

        Ingredient::create([
            'name' => 'atún',
            'description' => 'es pescado',
            'isvegan' => false,
            'isvegetarian' => false
        ]);

        Ingredient::create([
            'name' => 'mantequilla',
            'description' => 'mantequilla',
            'isvegan' => false,
            'isvegetarian' => true
        ])->allergens()->attach([
            'allergen_id' => $lacteo
        ]);

        Ingredient::create([
            'name' => 'queso mozarella',
            'description' => 'derivado del leche',
            'isvegan' => false,
            'isvegetarian' => true
        ])->allergens()->attach([
            'allergen_id' => $lacteo
        ]);

        Ingredient::create([
            'name' => 'queso Gouda',
            'description' => 'derivado del leche',
            'isvegan' => false,
            'isvegetarian' => true
        ])->allergens()->attach([
            'allergen_id' => $lacteo
        ]);

        Ingredient::create([
            'name' => 'queso Brie',
            'description' => 'derivado del leche',
            'isvegan' => false,
            'isvegetarian' => true
        ])->allergens()->attach([
            'allergen_id' => $lacteo
        ]);

        Ingredient::create([
            'name' => 'queso de cabra',
            'description' => 'derivado del leche',
            'isvegan' => false,
            'isvegetarian' => true
        ])->allergens()->attach([
            'allergen_id' => $lacteo
        ]);

        Ingredient::create([
            'name' => 'tomate',
            'description' => 'tomate rojo',
            'isvegan' => true,
            'isvegetarian' => true
        ]);

        Ingredient::create([
            'name' => 'pepino',
            'description' => 'pepino',
            'isvegan' => true,
            'isvegetarian' => true
        ]);

        Ingredient::create([
            'name' => 'caldo de carne',
            'description' => 'caldo de carne',
            'isvegan' => false,
            'isvegetarian' => false
        ]);

        Ingredient::create([
            'name' => 'nueces',
            'description' => 'nueces',
            'isvegan' => true,
            'isvegetarian' => true
        ])->allergens()->attach([
            'allergen_id' => $frutoseco
        ]);

        Ingredient::create([
            'name' => 'azúcar',
            'description' => 'azúcar blanco refinado de caña',
            'isvegan' => true,
            'isvegetarian' => true
        ]);

        Ingredient::create([
            'name' => 'esencia de vainilla',
            'description' => "extracto de vainilla",
            'isvegan' => true,
            'isvegetarian' => true
        ]);

        Ingredient::create([
            'name' => 'bicarbonato',
            'description' => "bicarbonato",
            'isvegan' => true,
            'isvegetarian' => true
        ]);

        Ingredient::create([
            'name' => 'galletas maría',
            'description' => 'galletas típicas. Pueden contener leche',
            'isvegan' => false,
            'isvegetarian' => true
        ])->allergens()->attach([
            'allergen_id' => $gluten
        ]);

        Ingredient::create([
            'name' => 'cacao en polvo',
            'description' => 'Cacao pulverizado',
            'isvegan' => true,
            'isvegetarian' => true
        ]);

        Ingredient::create([
            'name' => 'aceite de oliva',
            'description' => 'Aceite de oliva virgen extra',
            'isvegan' => true,
            'isvegetarian' => true
        ]);

        Ingredient::create([
            'name' => 'zumo de limón',
            'description' => 'zumo de limón natural',
            'isvegan' => true,
            'isvegetarian' => true
        ]);

        Ingredient::create([
            'name' => 'cebolla',
            'description' => 'Cebolla normal',
            'isvegan' => true,
            'isvegetarian' => true
        ]);

        Ingredient::create([
            'name' => 'apio',
            'description' => 'Apio normal',
            'isvegan' => true,
            'isvegetarian' => true
        ]);

        Ingredient::create([
            'name' => 'zanahoria',
            'description' => 'Zanahoria normal',
            'isvegan' => true,
            'isvegetarian' => true
        ]);

        Ingredient::create([
            'name' => 'garbanzo(s)',
            'description' => 'Garbanzos normales',
            'isvegan' => true,
            'isvegetarian' => true
        ]);

        Ingredient::create([
            'name' => 'pimiento verde',
            'description' => 'Pimientos verdes normales',
            'isvegan' => true,
            'isvegetarian' => true
        ]);

        Ingredient::create([
            'name' => 'pimiento rojo',
            'description' => 'Pimientos rojos normales',
            'isvegan' => true,
            'isvegetarian' => true
        ]);

        Ingredient::create([
            'name' => 'ajo',
            'description' => 'Gajo de ajo',
            'isvegan' => true,
            'isvegetarian' => true
        ]);

        Ingredient::create([
            'name' => 'jengibre',
            'description' => 'Jengibre fresco',
            'isvegan' => true,
            'isvegetarian' => true
        ]);

        Ingredient::create([
            'name' => 'champiñón(es)',
            'description' => 'champiñones normales',
            'isvegan' => true,
            'isvegetarian' => true
        ]);

        Ingredient::create([
            'name' => 'vinagre de vino blanco',
            'description' => 'vinagre normal',
            'isvegan' => true,
            'isvegetarian' => true
        ]);

        Ingredient::create([
            'name' => 'vinagre de jerez',
            'description' => 'vinagre de jerez',
            'isvegan' => true,
            'isvegetarian' => true
        ]);

        Ingredient::create([
            'name' => 'vino blanco',
            'description' => 'Vino blanco',
            'isvegan' => true,
            'isvegetarian' => true
        ]);

        Ingredient::create([
            'name' => 'vino tinto',
            'description' => 'Vino tinto',
            'isvegan' => true,
            'isvegetarian' => true
        ]);

        Ingredient::create([
            'name' => 'arroz blanco',
            'description' => 'arroz blanco normal',
            'isvegan' => true,
            'isvegetarian' => true
        ]);

        Ingredient::create([
            'name' => 'crema pastelera',
            'description' => 'crema pastelera dulce',
            'isvegan' => false,
            'isvegetarian' => true
        ])->allergens()->attach([
            1 => ['allergen_id' => $lacteo],
            2 => ['allergen_id' => $gluten ]
        ]);

        Ingredient::create([
            'name' => 'agua',
            'description' => 'agua potable',
            'isvegan' => true,
            'isvegetarian' => true
        ]);

        Ingredient::create([
            'name' => 'fresas',
            'description' => 'agua potable',
            'isvegan' => true,
            'isvegetarian' => true
        ]);

        Ingredient::create([
            'name' => 'gelatina',
            'description' => 'gelatina comestible sin sabor',
            'isvegan' => false,
            'isvegetarian' => false
        ]);

        Ingredient::create([
            'name' => 'masa de hojaldre',
            'description' => 'masa de hojaldre',
            'isvegan' => false,
            'isvegetarian' => false
        ]);

        Ingredient::create([
            'name' => 'soja texturizada fina',
            'description' => 'soja texturizada fina',
            'isvegan' => true,
            'isvegetarian' => true
        ]);

        Ingredient::create([
            'name' => 'salsa de soja',
            'description' => 'salsa de soja',
            'isvegan' => true,
            'isvegetarian' => true
        ]);

        Ingredient::create([
            'name' => 'tabasco',
            'description' => 'tabasco',
            'isvegan' => true,
            'isvegetarian' => true
        ]);

        Ingredient::create([
            'name' => 'sal',
            'description' => 'sal fino',
            'isvegan' => true,
            'isvegetarian' => true
        ]);

        Ingredient::create([
            'name' => 'curry',
            'description' => 'curry en especia',
            'isvegan' => true,
            'isvegetarian' => true
        ]);

        Ingredient::create([
            'name' => 'ternera',
            'description' => 'carne de ternera',
            'isvegan' => false,
            'isvegetarian' => false
        ]);

        Ingredient::create([
            'name' => 'pan',
            'description' => 'pan de tu preferencia',
            'isvegan' => false,
            'isvegetarian' => true
        ])->allergens()->attach([
            'allergen_id' => $gluten
        ]);

        Ingredient::create([
            'name' => 'leche de arroz',
            'description' => 'leche de arroz',
            'isvegan' => true,
            'isvegetarian' => true
        ]);

        Ingredient::create([
            'name' => 'leche de coco',
            'description' => 'leche de coco',
            'isvegan' => true,
            'isvegetarian' => true
        ]);

        Ingredient::create([
            'name' => 'canela',
            'description' => 'canela',
            'isvegan' => true,
            'isvegetarian' => true
        ]);

        Ingredient::create([
            'name' => 'limón',
            'description' => 'limón',
            'isvegan' => true,
            'isvegetarian' => true
        ]);

        Ingredient::create([
            'name' => 'sirope de arroz',
            'description' => 'sirope de arroz',
            'isvegan' => true,
            'isvegetarian' => true
        ]);


        Ingredient::create([
            'name' => 'harina de maíz ',
            'description' => 'harina de maíz ',
            'isvegan' => true,
            'isvegetarian' => true
        ])->allergens()->attach([
            'allergen_id' => $gluten
        ]);

        Ingredient::create([
            'name' => 'agar-agar en polvo',
            'description' => 'agar-agar en polvo',
            'isvegan' => true,
            'isvegetarian' => true
        ]);

        Ingredient::create([
            'name' => 'azúcar de coco',
            'description' => 'azúcar de coco',
            'isvegan' => true,
            'isvegetarian' => true
        ]);

        Ingredient::create([
            'name' => 'pimienta',
            'description' => 'la pimienta de tu preferencia, recomendamos las rojas',
            'isvegan' => true,
            'isvegetarian' => true
        ]);

        Ingredient::create([
            'name' => 'caramelo líquido',
            'description' => 'Caramelo líquido para postres.',
            'isvegan' => true,
            'isvegetarian' => true
        ]);

        Ingredient::create([
            'name' => 'refresco de cola',
            'description' => 'Refresco de cola.',
            'isvegan' => true,
            'isvegetarian' => true
        ]);

        Ingredient::create([
            'name' => 'vinagre de módena',
            'description' => 'Vinagre balsámico de módena.',
            'isvegan' => true,
            'isvegetarian' => true
        ]);

        Ingredient::create([
            'name' => 'ketchup',
            'description' => 'Ketchup de bote.',
            'isvegan' => true,
            'isvegetarian' => true
        ]);

        Ingredient::create([
            'name' => 'carne picada de vacuno',
            'description' => 'Carne picada de vacuno.',
            'isvegan' => false,
            'isvegetarian' => false
        ]);

        Ingredient::create([
            'name' => 'cebolla caramelizada',
            'description' => 'Cebolla caramelizada dulce.',
            'isvegan' => true,
            'isvegetarian' => true
        ]);

        Ingredient::create([
            'name' => 'pan de hamburguesa',
            'description' => 'Pan con la forma ideal para recetas de hamburguesa',
            'isvegan' => true,
            'isvegetarian' => true
        ]);

        Ingredient::create([
            'name' => 'Chimichurri',
            'description' => 'Chimichurri de bote, en forma de especia',
            'isvegan' => true,
            'isvegetarian' => true
        ]);

        Ingredient::create([
            'name' => 'Canónigos',
            'description' => 'Canónigos frescos',
            'isvegan' => true,
            'isvegetarian' => true
        ]);

        for ($x = 0; $x < 15; $x++) {
            Ingredient::create([
                'name' => $faker->text($maxNbChars=15),
                'description' =>  $faker->text($maxNbChars=15),
                'isvegan' => $faker->boolean,
                'isvegetarian' => $faker->boolean
            ]);
        }
    }
}
