<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // primero los tipos genericos
        $this->call(MeasureUnitTypeSeeder::class);
        $this->call(MeasureUnitSeeder::class);
        $this->call(AllergensSeeder::class);
        $this->call(IngredientsTableSeeder::class);
        $this->call(RecipesTableSeeder::class);
        //i lost this file and need to recreate
        $this->call(TagsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(UsersSearchesTableSeeder::class);
        //$this->call(FavRecipesTableSeeder::class);
        // PONER AQUÍ LOS SEEDERS QUE SE DESEA UTILIZAR!!
        // El orden de ejecución debe ser coherente a las relaciones entre tablas.
        // Para generar seeders, `php artisan make:seed [nombreseedclass]`
        // Para ejecutar los seeders, `php artisan db:seed`
    }
}
