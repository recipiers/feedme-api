<?php

use Illuminate\Database\Seeder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Model\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // User::truncate();

        $faker = \Faker\Factory::create();

        $password = hash('sha256', 'p4ssw0rd');

        // Crear falso admin de prueba
        User::create([
            'name' => 'admin1',
            'email' => 'admin1@fakemail.com',
            'isAdmin' => true,
            'password' => $password
        ]);

        // Crear 10 falsos usuarios para probar
        for ($i = 0; $i < 10; $i++) {
            User::create([
                'name' => $faker->name,
                'email' => $faker->email,
                'isAdmin' => false,
                'password' => $password
            ]);
        }
    }
}
