<?php

use App\Model\MeasureUnit;
use App\Model\MeasureUnitType;
use Illuminate\Database\Seeder;
use Faker\Factory;

class MeasureUnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // MeasureUnit::truncate();
        $faker = Factory::create();

        $peso = MeasureUnitType::where('name','masa')->first();
        $volumen = MeasureUnitType::where('name','volumen')->first();
        $unidades = MeasureUnitType::where('name','unidad')->first();

        $peso->genericUnit()->create([
            'name' => 'gramo(s)',
            'genericequivalent' => 1,
            'symbol' => 'g.'
        ]);

        $peso->measureUnits()->create([
            'name' => 'kilogramo(s)',
            'genericequivalent' => 1000.0,
            'symbol' => 'kg.'
        ]);

        $peso->measureUnits()->create([
            'name' => 'onza(s)',
            'genericEquivalent' => 28.3495231,
            'symbol' => 'oz.'
        ]);

        $peso->measureUnits()->create([
            'name' => 'libra(s)',
            'genericEquivalent' => 453.59237,
            'symbol' => 'lb.'
        ]);

        $volumen->genericUnit()->create([
            'name' => 'mililitro(s)',
            'genericequivalent' => 1.0,
            'symbol' => 'ml'
        ]);

        $volumen->measureUnits()->create([
            'name' => 'Litros',
            'genericequivalent' => 1000.0,
            'symbol' => 'L.'
        ]);

        $volumen->measureUnits()->create([
            'name' => 'vaso(s)',
            'genericequivalent' => 200.0,
            'symbol' => 'vaso(s)'
        ]);

        $volumen->measureUnits()->create([
            'name' => 'vasito(s)',
            'genericequivalent' => 100.0,
            'symbol' => 'vasito(s)'
        ]);

        $volumen->measureUnits()->create([
            'name' => 'tazón(es)',
            'genericequivalent' => 250.0,
            'symbol' => 'tazón(es)'
        ]);

        $volumen->measureUnits()->create([
            'name' => 'taza(s)',
            'genericequivalent' => 150.0,
            'symbol' => 'taza(s)'
        ]);

        $volumen->measureUnits()->create([
            'name' => 'tacita(s)',
            'genericequivalent' => 100.0,
            'symbol' => 'tacita(s)'
        ]);



        $volumen->measureUnits()->create([
            'name' => 'cucharada(s)',
            'genericequivalent' => 15.0,
            'symbol' => 'cucharada(s)'
        ]);

        $volumen->measureUnits()->create([
            'name' => 'cucharadita(s)',
            'genericequivalent' => 5.0,
            'symbol' => 'cucharadita(s)'
        ]);

        $volumen->measureUnits()->create([
            'name' => 'gota(s)',
            'genericequivalent' => 0.05,
            'symbol' => 'gota(s)'
        ]);

        $unidades->genericUnit()->create([
            'name' => 'unidad(es)',
            'genericequivalent' => 1,
            'symbol' => 'u.'
        ]);

        $unidades->measureUnits()->create([
            'name' => 'medida a gusto',
            'genericequivalent' => 1,
            'symbol' => 'medida a gusto de'
        ]);

        $unidades->genericUnit()->create([
            'name' => 'trocito',
            'genericequivalent' => 1,
            'symbol' => 'trocito'
        ]);

        $unidades->measureUnits()->create([
            'name' => 'ralladura',
            'genericequivalent' => 1,
            'symbol' => 'ralladura'
        ]);

        $unidades->measureUnits()->create([
            'name' => 'rama',
            'genericequivalent' => 1,
            'symbol' => 'rama'
        ]);

        $unidades->measureUnits()->create([
            'name' => 'chorro',
            'genericequivalent' => 1,
            'symbol' => 'chorro'
        ]);


        $unidades->measureUnits()->create([
            'name' => 'una pizca',
            'genericequivalent' => 1,
            'symbol' => 'una pizca'
        ]);

    }
}
