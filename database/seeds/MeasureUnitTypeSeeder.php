<?php

use App\Model\MeasureUnitType;
use App\Model\MeasureUnit;

use Faker\Factory;

use Illuminate\Database\Seeder;

class MeasureUnitTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // MeasureUnitType::truncate();
        $faker = Factory::create();

        MeasureUnitType::create([
            'name' => 'masa'
        ]);

        MeasureUnitType::create([
            'name' => 'volumen'
        ]);

        MeasureUnitType::create([
            'name' => 'unidad'
        ]);

        MeasureUnitType::create([
            'name' => 'otros'
        ]);
    }
}
