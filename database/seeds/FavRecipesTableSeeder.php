<?php

use App\Model\User;
use Illuminate\Database\Seeder;

class FavRecipesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::find(2)->favouriteRecipes()->attach(['recipe_id'=>1]);
        User::find(2)->favouriteRecipes()->attach(['recipe_id'=>2]);
        User::find(1)->favouriteRecipes()->attach(['recipe_id'=>3]);
        User::find(1)->favouriteRecipes()->attach(['recipe_id'=>4]);

    }
}
