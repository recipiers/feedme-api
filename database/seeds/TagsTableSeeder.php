<?php

use Illuminate\Database\Seeder;
use App\Model\Recipe;
use App\Model\Tag;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        Tag::truncate();
        $tags = ["lorem","ipsum","catalana","argentina","brasileña","francesa","italiana","postre","aperitivo","dieta del mediteraneo","bajos carboidratos"];
        for($x =0; $x <120; $x++) {
            $randomIndex = array_rand($tags);
            // put a random tag into a random recipe, if it already has this tag ignore
            try {
                Recipe::all()->random()->tags()->create(['tagname'=> $tags[$randomIndex] ]);
            } catch (Exception $e) {
                continue;
            }
        }
    }
}
