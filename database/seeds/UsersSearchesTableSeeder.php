<?php

use App\Model\User;
use App\Model\Search;
use App\Model\Ingredient;
use Illuminate\Database\Seeder;

class UsersSearchesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* OLD SEEDER IGNORE
        $faker = Faker\Factory::create();
        Search::truncate();
        $users = User::all()->count();

        // usuario 1 es admin y sin busquedas
        // el user 2 tendra una busqueda sin tiempo maximo

        User::find(2)->searches()->create([
            'name' => $faker->text($maxNbChars = 25),
            'isvegan' => $faker->boolean,
            'isvegetarian' => $faker->boolean
        ]);

        for ($x=3; $x < $users; $x++) {
            // no todos los usuarios tendran busquedas
            if ($x%3 != 0) {
                User::find($x)->searches()->create([
                    'name' => $faker->text($maxNbChars = 25),
                    'isvegan' => $faker->boolean,
                    'isvegetarian' => $faker->boolean,
                    'maxtime' => $faker->numberBetween($min=20, $max=40)
                ]);
            }
        }*/
        $user2 = User::find(2);
        $user3 = User::find(3);

        $user2->searches()->create([
            'title'=>'veganas rapidas',
            'isvegan'=>true,
            'maxtime'=>60
        ]);
        $user2->searches()->create([
            'title'=>'fast',
            'maxtime'=>30
        ]);
        $user2->searches()->create([
            'title'=>'pizzas',
            'name'=>'pizza'
        ]);

        $arroz = Ingredient::where('name','like','%arroz%')->get()->pluck('id')->toArray()[0];

        $user3->searches()->create([
            'title'=>'Recetas de arroces'
        ])->ingredients()->attach(['ingredient_id'=>$arroz]);

        $user3->searches()->create([
            'title'=>'comidas italianas'
        ]);


    }
}
