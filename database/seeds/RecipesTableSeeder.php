<?php

use App\Model\Ingredient;
use App\Model\MeasureUnit;
use App\Model\Recipe;
use App\Model\Step;
use Faker\Factory;
use Illuminate\Database\Seeder;

class RecipesTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createRecipe([
            "name" => 'Tortilla francesa',
            "description" => 'Receta simple de tortilla francesa',
            "time" => 5,
            "pictureurl" => 'https://vdmedia.elpais.com/elpaistop/201711/8/201711081227871_1510141189_asset_still.png',
            "numportions" => 1,
            "difficulty" => 1,
            "ingredients" => [
                [
                    "ingredient_id" => $this->ingredientID('huevos'),
                    "amount" => 1,
                    "measureunit_id" => $this->measureID('u.'),
                ]
            ],
            "steps" => [
                'Poner una sartén en el fuego y echarle un chorrito de aceite',
                'Una vez el aceite se ha calentado, echarle un huevo.',
                'Remover los huevos y esperar a que cuajen. Se puede dejar más o menos tiempo, en función del nivel de cocción deseado.',
            ]
        ]);

        $this->createRecipe([
            "name" => "Tomates secos",
            "description" => "Super saludable y facil",
            "time" => 30,
            "pictureurl" => "http://cdn.elcocinerocasero.com/imagen/receta/1000/2015-11-11-19-02-41/tomates-secos-en-aceite-de-oliva.jpeg",
            "numportions" => 2,
            "difficulty" => 3,
            "steps" => ["ponga en el horno por media hora hasta que seque", "ponga aceite", "listo"],
            "ingredients" => [
                [
                    "ingredient_id" => $this->ingredientID('tomate'),
                    "measureunit_id" => $this->measureID('vaso'),
                    "amount" => 2,
                ],
            ],
        ]);

        $this->createRecipe([
            "name" => "Tarta de fresas y crema pastelera",
            "description" => "Esta una esas recetas de postre que es un claro ejemplo de que no hace falta ser un cocinillas pro para hacer una tarta deliciosa.

            Una demostración práctica de que, en casa, sin muchos conocimientos de cocina, con sólo un poco de tiempo, podemos preparar postres y dulces de rechupete.

            Esta tarta consta de dos preparaciones, a cual más sencilla. Por una lado una crema pastelera súper fácil  de preparar y muy rica.",
            "time" => 60,
            "pictureurl" => "https://www.recetasderechupete.com/wp-content/uploads/2016/07/tarta_fresas_crema_pastelera-525x360.jpg",
            "numportions" => 10,
            "difficulty" => 4,
            "steps" => [
                "Comenzamos preparando la crema pastelera para tenerla lista cuando nos pongamos con la tarta. Es importante que hagamos una crema más bien espesa, para que se aguante bien al cortar la tarta",
                "Podemos hacerlo con cierta antelación, incluso el día anterior, así distribuimos el trabajo.",
                "Extendemos la lámina de hojaldre sobre un molde redondo.",
                "Mantenemos bajo la masa el papel vegetal que viene con el paquete. Si os apetece una tarta casera 100%, podéis animaros y preparar vosotros mismos la masa de hojaldre.",
                "Aunque da cierto trabajo el resultado vale la pena. En el blog podéis encontrar una receta, paso a paso, para prepararla sin problemas.",
                "Pinchamos con un tenedor toda la base de la tarta y, con el horno previamente caliente a 180º C.",
                "Horneamos la base de hojaldre durante unos 20 minutos, hasta que esté dorada.",
                "Os aconsejo que estar muy pendientes de la masa en el horno.",
                "Seguramente, a pesar de haberla pinchado, la masa tenderá a hincharse durante el horneado. Cuando así sea tenéis que abrir el horno, volvéis a pinchar con un tenedor y listo. Así no subirá.",
                "Apresentacion final:",
                "Mientras se hornea la base de hojaldre, hidratamos las hojas de gelatina para hacer el brillo protector de la tarta. En un cazo calentamos el agua con el azúcar. Dejamos que cueza a fuego medio bajo durante unos 5 minutos.",
                "Apagamos el fuego y añadimos las hojas de gelatina ya hidratadas. Mezclamos bien para que se deshagan y reservamos. Debemos dejar que el almíbar se enfríe totalmente y comience a espesar antes de usarlo, por eso es aconsejable hacerlo con tiempo.",
                "Cuando el almíbar esté totalmente frío, si queremos podemos introducirlo unos minutos en la nevera para ayudarle a que espese.",
                "Cuando tengamos todas las elaboraciones listas comenzamos con el montaje de la tarta. Rellenamos la base de hojaldre con una capa de crema pastelera. Podemos hacerlo con una cuchara o distribuirlo de forma más sencilla con una manga pastelera.",
                "Cubrimos la tarta con las fresas con la forma y decoración que queramos, ya sean enteras o fileteadas. Cuando el brillo para tartas esté ligeramente espeso, comenzamos a pintar toda la superficie de la tarta con él ayudándonos de un pincel de cocina.",

            ],
            "ingredients" => [
                [
                    "ingredient_id" => $this->ingredientID('fresa'),
                    "measureunit_id" => $this->measureID('vaso'),
                    "amount" => 1,
                ],
            ],
        ]);

        $this->createRecipe([
            "name" => "Tarta de fresas y crema pastelera",
            "description" => "Esta una esas recetas de postre que es un claro ejemplo de que no hace falta ser un cocinillas pro para hacer una tarta deliciosa.

            Una demostración práctica de que, en casa, sin muchos conocimientos de cocina, con sólo un poco de tiempo, podemos preparar postres y dulces de rechupete.

            Esta tarta consta de dos preparaciones, a cual más sencilla. Por una lado una crema pastelera súper fácil  de preparar y muy rica.",
            "time" => 60,
            "pictureurl" => "https://www.recetasderechupete.com/wp-content/uploads/2016/07/tarta_fresas_crema_pastelera-525x360.jpg",
            "numportions" => 10,
            "difficulty" => 4,
            "steps" => [
                "Comenzamos preparando la crema pastelera para tenerla lista cuando nos pongamos con la tarta. Es importante que hagamos una crema más bien espesa, para que se aguante bien al cortar la tarta",
                "Podemos hacerlo con cierta antelación, incluso el día anterior, así distribuimos el trabajo.",
                "Extendemos la lámina de hojaldre sobre un molde redondo.",
                "Mantenemos bajo la masa el papel vegetal que viene con el paquete. Si os apetece una tarta casera 100%, podéis animaros y preparar vosotros mismos la masa de hojaldre.",
                "Aunque da cierto trabajo el resultado vale la pena. En el blog podéis encontrar una receta, paso a paso, para prepararla sin problemas.",
                "Pinchamos con un tenedor toda la base de la tarta y, con el horno previamente caliente a 180º C.",
                "Horneamos la base de hojaldre durante unos 20 minutos, hasta que esté dorada.",
                "Os aconsejo que estar muy pendientes de la masa en el horno.",
                "Seguramente, a pesar de haberla pinchado, la masa tenderá a hincharse durante el horneado. Cuando así sea tenéis que abrir el horno, volvéis a pinchar con un tenedor y listo. Así no subirá.",
                "Apresentacion final:",
                "Mientras se hornea la base de hojaldre, hidratamos las hojas de gelatina para hacer el brillo protector de la tarta. En un cazo calentamos el agua con el azúcar. Dejamos que cueza a fuego medio bajo durante unos 5 minutos.",
                "Apagamos el fuego y añadimos las hojas de gelatina ya hidratadas. Mezclamos bien para que se deshagan y reservamos. Debemos dejar que el almíbar se enfríe totalmente y comience a espesar antes de usarlo, por eso es aconsejable hacerlo con tiempo.",
                "Cuando el almíbar esté totalmente frío, si queremos podemos introducirlo unos minutos en la nevera para ayudarle a que espese.",
                "Cuando tengamos todas las elaboraciones listas comenzamos con el montaje de la tarta. Rellenamos la base de hojaldre con una capa de crema pastelera. Podemos hacerlo con una cuchara o distribuirlo de forma más sencilla con una manga pastelera.",
                "Cubrimos la tarta con las fresas con la forma y decoración que queramos, ya sean enteras o fileteadas. Cuando el brillo para tartas esté ligeramente espeso, comenzamos a pintar toda la superficie de la tarta con él ayudándonos de un pincel de cocina.",

            ],
            "ingredients" => [
                [
                    "ingredient_id" => $this->ingredientID('fresa'),
                    "measureunit_id" => $this->measureID('vaso'),
                    "amount" => 1,
                ],
                [
                    "ingredient_id" => $this->ingredientID('crema pastelera'),
                    "measureunit_id" => $this->measureID('ml'),
                    "amount" => 500,
                ], [
                    "ingredient_id" => $this->ingredientID('agua'),
                    "measureunit_id" => $this->measureID('ml'),
                    "amount" => 250,
                ], [
                    "ingredient_id" => $this->ingredientID('azúcar'),
                    "measureunit_id" => $this->measureID('cucharada'),
                    "amount" => 3,
                ], [
                    "ingredient_id" => $this->ingredientID('gelatina'),
                    "measureunit_id" => $this->measureID('g.'),
                    "amount" => 6,
                ], [
                    "ingredient_id" => $this->ingredientID('masa de hojaldre'),
                    "measureunit_id" => $this->measureID('u.'),
                    "amount" => 1,
                ],
            ],
        ]);

        $this->createRecipe([
            "name" => "Nuggets vegetarianos",
            "description" => "Aunque cuando hablamos de platos vegetarianos nos suelen venir a la cabeza verduras y platos saludables, lo cierto es que también se pueden preparar de vez en cuando “guarrerías” vegetarianas que no tienen nada que envidiar a las carnívoras como es el caso de estos nuggets vegetarianos, que están hechos con soja texturizada y que son una buena alternativa a los nuggets de pollo para aquellos que no pueden o no quieren consumir carne.

            Son facilísimos de hacer y a mí, pese a no ser vegetariana, me encantan, pues como ya os he contado en alguna ocasión, la soja texturizada tiene la ventaja de que es baratísima y se conserva en la alacena durante meses, por lo que me encanta tener una bolsa para echar mano de ella cuando hace falta.",
            "time" => 20,
            "pictureurl" => "https://s2.eestatic.com/2016/03/08/cocinillas/Cocinillas_108001060_116246436_1706x960.jpg",
            "numportions" => 2,
            "difficulty" => 1,
            "steps" => [
                "En un bol mezclamos el agua con la salsa de soja y el tabasco, echamos la soja texturizada y la dejamos a remojo durante al menos 15 minutos durante los que absorberá todo el líquido.",
                "Pasado ese tiempo, en otro bol, batimos un huevo son sal y pimienta al gusto, añadimos la soja bien escurrida y 80 g de pan rallado, amasamos hasta tener una masa que se pueda compactar con las manos. Si fuese necesario añadimos un poco más de pan rallado.",
                "Cuando tengamos una masa que no se desmigue demasiado, vamos formando tortitas con ayuda de las manos y friéndolas en abundante aceite caliente hasta que se doren por ambos lados.",
                "Según se vayan haciendo las retiramos a un colador para que escurran y servimos calientes.",
            ],
            "ingredients" => [
                [
                    "ingredient_id" => $this->ingredientID('soja texturizada'),
                    "measureunit_id" => $this->measureID('g.'),
                    "amount" => 100,
                ],
                [
                    "ingredient_id" => $this->ingredientID('agua'),
                    "measureunit_id" => $this->measureID('ml'),
                    "amount" => 100,
                ], [
                    "ingredient_id" => $this->ingredientID('salsa de soja'),
                    "measureunit_id" => $this->measureID('ml'),
                    "amount" => 50,
                ], [
                    "ingredient_id" => $this->ingredientID('tabasco'),
                    "measureunit_id" => $this->measureID('gota'),
                    "amount" => 5,
                ], [
                    "ingredient_id" => $this->ingredientID('huevo'),
                    "measureunit_id" => $this->measureID('u.'),
                    "amount" => 1,
                ], [
                    "ingredient_id" => $this->ingredientID('sal'),
                    "measureunit_id" => $this->measureID('medida a gusto de'),
                    "amount" => 1,
                ], [
                    "ingredient_id" => $this->ingredientID('pimienta'),
                    "measureunit_id" => $this->measureID('medida a gusto de'),
                    "amount" => 1,
                ], [
                    "ingredient_id" => $this->ingredientID('pan'),
                    "measureunit_id" => $this->measureID('g.'),
                    "amount" => 80,
                ], [
                    "ingredient_id" => $this->ingredientID('aceite'),
                    "measureunit_id" => $this->measureID('medida a gusto de'),
                    "amount" => 1,
                ],
            ],
        ]);

        $this->createRecipe([
            "name" => "Crema Catalana Vegana",
            "description" => "Cuando uno decide hacerse vegano se limita lo que puede comer en pro del bienestar animal. Una de las cosas que deja de comer son los huevos y leche, así que vamos a hacer una crema catalana vegana para chuparse los dedos. Una crema catalana sin huevo y sin leche, 100% vegetal y que sabe como la crema catalana original.",
            "time" => 40,
            "pictureurl" => "https://jardindeluna.com/wp-content/uploads/crema-catalana-primavera-opt.jpg",
            "numportions" => 4,
            "difficulty" => 2,
            "steps" => [
                "Colocamos la bebida vegetal en un cazo.",
                "Añadimos el agar-agar, la rama de canela, la ralladura de limón, la melaza o el azúcar, y removemos bien con la ayuda de unas varillas.",
                "Ponemos el cazo al fuego y lo llevamos a ebullición, bajamos el fuego, y removemos para evitar que se pegue. ",
                "Añadimos la harina y la sal previamente diluidas en un poco de agua, y seguimos cocinado a fuego lento, removiendo la mezcla unos minutos más.",
                "Apagamos el fuego y retiramos, dejando que se temple unos 5 minutos. ",
                "Sacamos la rama de canela y servimos en los recipientes elegidos. ",
                "Espolvoreamos el azúcar de coco y la canela, y guardamos en la nevera un mínimo de 3 horas. ",
                "Sacamos de la nevera una media hora antes de consumir. ",
            ],
            "ingredients" => [
                [
                    "ingredient_id" => $this->ingredientID('leche de arroz'),
                    "measureunit_id" => $this->measureID('ml'),
                    "amount" => 500,
                ],
                [
                    "ingredient_id" => $this->ingredientID('canela'),
                    "measureunit_id" => $this->measureID('rama'),
                    "amount" => 1,
                ], [
                    "ingredient_id" => $this->ingredientID('limón'),
                    "measureunit_id" => $this->measureID('ralladura'),
                    "amount" => 1,
                ], [
                    "ingredient_id" => $this->ingredientID('sirope de arroz'),
                    "measureunit_id" => $this->measureID('g.'),
                    "amount" => 50,
                ], [
                    "ingredient_id" => $this->ingredientID('harina de maíz'),
                    "measureunit_id" => $this->measureID('g.'),
                    "amount" => 20,
                ], [
                    "ingredient_id" => $this->ingredientID('agar-agar en polvo'),
                    "measureunit_id" => $this->measureID('g.'),
                    "amount" => 1,
                ], [
                    "ingredient_id" => $this->ingredientID('azúcar de coco'),
                    "measureunit_id" => $this->measureID('medida a gusto'),
                    "amount" => 1,
                ], [
                    "ingredient_id" => $this->ingredientID('sal'),
                    "measureunit_id" => $this->measureID('medida a gusto'),
                    "amount" => 1,
                ],
            ],
        ]);

        $this->createRecipe([
            "name" => "Crema Catalana Vegana",
            "description" => "Cuando uno decide hacerse vegano se limita lo que puede comer en pro del bienestar animal. Una de las cosas que deja de comer son los huevos y leche, así que vamos a hacer una crema catalana vegana para chuparse los dedos. Una crema catalana sin huevo y sin leche, 100% vegetal y que sabe como la crema catalana original.",
            "time" => 40,
            "pictureurl" => "https://jardindeluna.com/wp-content/uploads/crema-catalana-primavera-opt.jpg",
            "numportions" => 4,
            "difficulty" => 2,
            "steps" => [
                "Colocamos la bebida vegetal en un cazo.",
                "Añadimos el agar-agar, la rama de canela, la ralladura de limón, la melaza o el azúcar, y removemos bien con la ayuda de unas varillas.",
                "Ponemos el cazo al fuego y lo llevamos a ebullición, bajamos el fuego, y removemos para evitar que se pegue. ",
                "Añadimos la harina y la sal previamente diluidas en un poco de agua, y seguimos cocinado a fuego lento, removiendo la mezcla unos minutos más.",
                "Apagamos el fuego y retiramos, dejando que se temple unos 5 minutos. ",
                "Sacamos la rama de canela y servimos en los recipientes elegidos. ",
                "Espolvoreamos el azúcar de coco y la canela, y guardamos en la nevera un mínimo de 3 horas. ",
                "Sacamos de la nevera una media hora antes de consumir. ",
            ],
            "ingredients" => [
                [
                    "ingredient_id" => $this->ingredientID('leche de arroz'),
                    "measureunit_id" => $this->measureID('ml'),
                    "amount" => 500,
                ],
                [
                    "ingredient_id" => $this->ingredientID('canela'),
                    "measureunit_id" => $this->measureID('rama'),
                    "amount" => 1,
                ], [
                    "ingredient_id" => $this->ingredientID('limón'),
                    "measureunit_id" => $this->measureID('ralladura'),
                    "amount" => 1,
                ], [
                    "ingredient_id" => $this->ingredientID('sirope de arroz'),
                    "measureunit_id" => $this->measureID('g.'),
                    "amount" => 50,
                ], [
                    "ingredient_id" => $this->ingredientID('harina de maíz'),
                    "measureunit_id" => $this->measureID('g.'),
                    "amount" => 20,
                ], [
                    "ingredient_id" => $this->ingredientID('agar-agar en polvo'),
                    "measureunit_id" => $this->measureID('g.'),
                    "amount" => 1,
                ], [
                    "ingredient_id" => $this->ingredientID('azúcar de coco'),
                    "measureunit_id" => $this->measureID('medida a gusto'),
                    "amount" => 1,
                ], [
                    "ingredient_id" => $this->ingredientID('sal'),
                    "measureunit_id" => $this->measureID('medida a gusto'),
                    "amount" => 1,
                ],
            ],
        ]);


        $this->createRecipe([
            "name" => "Champiñones al horno crujientes",
            "description" => "Hoy quiero proponeros una receta muy peculiar: champiñones al horno crujientes con sabor a bacon, el topping vegano para tus cremas. Con un marinado previo y un paso por el horno, los champiñones quedan crujientes y sabrosos, con una textura y aromas que recuerdan mucho al bacon.",
            "time" => 40,
            "pictureurl" => "https://jardindeluna.com/wp-content/uploads/crema-catalana-primavera-opt.jpg",
            "numportions" => 6,
            "difficulty" => 1,
            "steps" => [
                "Colocamos la bebida vegetal en un cazo.",
                "Añadimos el agar-agar, la rama de canela, la ralladura de limón, la melaza o el azúcar, y removemos bien con la ayuda de unas varillas.",
                "Ponemos el cazo al fuego y lo llevamos a ebullición, bajamos el fuego, y removemos para evitar que se pegue. ",
                "Añadimos la harina y la sal previamente diluidas en un poco de agua, y seguimos cocinado a fuego lento, removiendo la mezcla unos minutos más.",
                "Apagamos el fuego y retiramos, dejando que se temple unos 5 minutos. ",
                "Sacamos la rama de canela y servimos en los recipientes elegidos. ",
                "Espolvoreamos el azúcar de coco y la canela, y guardamos en la nevera un mínimo de 3 horas. ",
                "Sacamos de la nevera una media hora antes de consumir. ",
            ],
            "ingredients" => [
                [
                    "ingredient_id" => $this->ingredientID('leche de arroz'),
                    "measureunit_id" => $this->measureID('ml'),
                    "amount" => 500,
                ],
                [
                    "ingredient_id" => $this->ingredientID('canela'),
                    "measureunit_id" => $this->measureID('rama'),
                    "amount" => 1,
                ], [
                    "ingredient_id" => $this->ingredientID('limón'),
                    "measureunit_id" => $this->measureID('ralladura'),
                    "amount" => 1,
                ], [
                    "ingredient_id" => $this->ingredientID('sirope de arroz'),
                    "measureunit_id" => $this->measureID('g.'),
                    "amount" => 50,
                ], [
                    "ingredient_id" => $this->ingredientID('harina de maíz'),
                    "measureunit_id" => $this->measureID('g.'),
                    "amount" => 20,
                ], [
                    "ingredient_id" => $this->ingredientID('agar-agar en polvo'),
                    "measureunit_id" => $this->measureID('g.'),
                    "amount" => 1,
                ], [
                    "ingredient_id" => $this->ingredientID('azúcar de coco'),
                    "measureunit_id" => $this->measureID('medida a gusto'),
                    "amount" => 1,
                ], [
                    "ingredient_id" => $this->ingredientID('sal'),
                    "measureunit_id" => $this->measureID('medida a gusto'),
                    "amount" => 1,
                ],
            ],
        ]);

        $this->createRecipe([
            "name" => "Pudding de cacao",
            "description" => "Pudding de cacao simple de preparar para todos tus invitados.",
            "time" => 60,
            "pictureurl" => "https://img.taste.com.au/4TRyMICK/w720-h480-cfill-q80/taste/2016/11/chocolate-pudding-3643-1.jpeg",
            "numportions" => 6,
            "difficulty" => 1,
            "steps" => [
                "Precalentamos el horno a 180º.",
                "Molemos las galletas en un mortero, hasta que queden prácticamente pulverizadas.",
                "Mezclamos en un recipiente los huevos, la leche, las galletas pulverizadas, el cacao en polvo y la esencia de vainilla mediante una batidora, electrónica o manual.",
                "Una vez mezclado todo, introducir la mezcla en un recipiente apto para hornear (de silicona térmica, vidrio PIREX, etc.), al que previamente le añadiremos un chorro de caramelo líquido.",
                "Dejar la mezcla en el horno durante aproximadamente 35-40 minutos. Se puede clavar un cuchillo en el pudding para ver si la masa ha cuajado o no.",
                "Una vez hecho, dejar reposar 20 minutos.",
                "Se puede consumir en caliente una vez reposado o si se desea, se puede meter en el frigorífico durante unas horas para poder servirlo frío."
            ],
            "ingredients" => [
                [
                    "ingredient_id" => $this->ingredientID('cacao en polvo'),
                    "measureunit_id" => $this->measureID('g.'),
                    "amount" => 5,
                ],
                [
                    "ingredient_id" => $this->ingredientID('galletas maría'),
                    "measureunit_id" => $this->measureID('u.'),
                    "amount" => 3,
                ], [
                    "ingredient_id" => $this->ingredientID('leche'),
                    "measureunit_id" => $this->measureID('ml'),
                    "amount" => 250,
                ], [
                    "ingredient_id" => $this->ingredientID('huevos'),
                    "measureunit_id" => $this->measureID('u.'),
                    "amount" => 1,
                ], [
                    "ingredient_id" => $this->ingredientID('caramelo líquido'),
                    "measureunit_id" => $this->measureID('ml'),
                    "amount" => 25,
                ], [
                    "ingredient_id" => $this->ingredientID('esencia de vainilla'),
                    "measureunit_id" => $this->measureID('a gusto'),
                    "amount" => 1,
                ]
            ],
        ]);

        $this->createRecipe([
            "name" => "Salsa barbacoa",
            "description" => "Salsa barbacoa casera improvisada, ideal para acompañar carnes a la brasa.",
            "time" => 50,
            "pictureurl" => "https://www.hola.com/imagenes/cocina/recetas/20200204159692/salsa-barbacoa/0-777-986/salsa-barbacoa-m.jpg",
            "numportions" => 10,
            "difficulty" => 1,
            "steps" => [
                "Poner un cazo en la sartén con el refresco de cola a fuego medio.",
                "Cuando esté hirviendo, añadir el vinagre de módena",
                "Añadir el ketchup. La cantidad de ketchup puede variarse en función del espesor deseado para la salsa barbacoa.",
                "Añadir el pimiento verde triturado.",
                "Reducir a fuego lento y dejar hirviendo hasta que la mezcla se haya reducido y recupere la consistencia del ketchup.",
                "Una vez hecho, sacar del fuego, y dejar reposar 20 minutos.",
                "Conservar en nevera o consumir."
            ],
            "ingredients" => [
                [
                    "ingredient_id" => $this->ingredientID('refresco de cola'),
                    "measureunit_id" => $this->measureID('ml'),
                    "amount" => 30,
                ],
                [
                    "ingredient_id" => $this->ingredientID('pimiento verde'),
                    "measureunit_id" => $this->measureID('u.'),
                    "amount" => 1 / 10,
                ], [
                    "ingredient_id" => $this->ingredientID('vinagre de módena'),
                    "measureunit_id" => $this->measureID('cucharada'),
                    "amount" => 1 / 10,
                ], [
                    "ingredient_id" => $this->ingredientID('ketchup'),
                    "measureunit_id" => $this->measureID('chorro'),
                    "amount" => 1 / 10,
                ]
            ],
        ]);

        $this->createRecipe([
            "name" => "Hamburguesa completa FeedMeat",
            "description" => "Hamburguesa personalizada como celebración de la inauguración de la aplicación de FeedMe&copy;.",
            "time" => 15,
            "pictureurl" => "https://theluxonomist.es/wp-content/uploads/2015/07/hamburguesanysteak-1280x720.jpg",
            "numportions" => 1,
            "difficulty" => 2,
            "steps" => [
                "Cortar los quesos en cubos pequeños.",
                "Mezclar los quesos con la carne picada y añadir el chimichurri. Dar forma de hamburguesa",
                "Poner la hamburguesa, el huevo, el pan y el pimiento verde en la plancha, vuelta y vuelta",
                "Para el montaje de la hamburguesa, montar los ingredientes en el siguiente orden entre el pan: cebolla caramelizada, canónigos, pimiento verde, la carne de hamburguesa, pimiento y huevo.",
                "Servir con tu aperitivo favorito."
            ],
            "ingredients" => [
                [
                    "ingredient_id" => $this->ingredientID('carne picada'),
                    "measureunit_id" => $this->measureID('g.'),
                    "amount" => 200,
                ],
                [
                    "ingredient_id" => $this->ingredientID('pimiento verde'),
                    "measureunit_id" => $this->measureID('u.'),
                    "amount" => 1 / 10,
                ], [
                    "ingredient_id" => $this->ingredientID('queso de cabra'),
                    "measureunit_id" => $this->measureID('g.'),
                    "amount" => 25,
                ], [
                    "ingredient_id" => $this->ingredientID('queso Brie'),
                    "measureunit_id" => $this->measureID('g.'),
                    "amount" => 25,
                ], [
                    "ingredient_id" => $this->ingredientID('cebolla caramelizada'),
                    "measureunit_id" => $this->measureID('g.'),
                    "amount" => 15,
                ], [
                    "ingredient_id" => $this->ingredientID('huevo'),
                    "measureunit_id" => $this->measureID('u.'),
                    "amount" => 1,
                ], [
                    "ingredient_id" => $this->ingredientID('pimiento verde'),
                    "measureunit_id" => $this->measureID('u.'),
                    "amount" => 1,
                ]
            ],
        ]);

        $this->createRecipe([
            "name" => "Mayonesa casera",
            "description" => "Mayonesa casera (cortesía de Èlia). Tener en cuenta que es para consumir en la comida para la que se prepara.",
            "time" => 10,
            "pictureurl" => "https://s1.eestatic.com/2019/10/24/cocinillas/cocinar/Mayonesa-Salsa-Huevo-Aceite-Vinagre-Aprende_a_cocinar_439219613_136149364_1024x576.jpg",
            "numportions" => 1,
            "difficulty" => 2,
            "steps" => [
                "Abrir el huevo y verter clara y yema en un recipiente a medida.",
                "Añadir una pizca de sal.",
                "Verter un chorro de aceite en el recipiente de forma que el huevo quede cubierto.",
                "Batir con una 'minipimer' fijando en el fondo del recipiente. Cuando la mezcla empiece a adquirir un color blanco más homogéneo, ir subiendo. Esto último es importante para que cuaje.",
                "Hecho el paso anterior repetir bajar y subir la minipimer para remover la mezcla.",
                "Si los pasos anteriores se han realizado correctamente, la mayonesa debe haber quedado suficientemente espesa como para no caer de una cuchara rebosada. Siendo así, está lista para acompañar tus platos."
            ],
            "ingredients" => [
                [
                    "ingredient_id" => $this->ingredientID('sal'),
                    "measureunit_id" => $this->measureID('pizca'),
                    "amount" => 1,
                ], [
                    "ingredient_id" => $this->ingredientID('huevo'),
                    "measureunit_id" => $this->measureID('u.'),
                    "amount" => 1,
                ], [
                    "ingredient_id" => $this->ingredientID('aceite'),
                    "measureunit_id" => $this->measureID('chorro'),
                    "amount" => 1,
                ]
            ],
        ]);
    }

    private function measureID($symbol)
    {
        return MeasureUnit::where('symbol', 'like', '%' . $symbol . '%')->pluck('id')->toArray()[0];
    }

    private function ingredientID($name)
    {
        return Ingredient::where('name', 'like', '%' . $name . '%')->pluck('id')->toArray()[0];
    }

    /**
     * Crea una nueva receta a partir de los datos otorgados.
     */
    private function createRecipe(array $data)
    {
        $recipe = new Recipe();
        $recipe->name = $data['name'];
        $recipe->time = $data['time'];
        $recipe->difficulty = $data['difficulty'];
        $recipe->description = $data['description'];
        $recipe->pictureurl = $data['pictureurl'];
        $recipe->numportions = $data['numportions'];
        $recipe->save();
        // Insertar los pasos de la receta
        if ($data['steps'] && is_array($data['steps'])) {
            $index = 1;
            foreach ($data['steps'] as  $step) {
                $recipe->steps()->save(new Step([
                    'ordernum' => $index++,
                    'description' => $step
                ]));
            }
        }
        // Insertar la relación de ingredientes de la receta
        if ($data['ingredients'] && is_array($data['ingredients'])) {
            foreach ($data['ingredients'] as $ingredient) {
                $ingredientsRecipe = $recipe->ingredients()->pluck('ingredient_id')->toArray();
                if (!in_array($ingredient['ingredient_id'], $ingredientsRecipe)) {
                    $recipe->ingredients()->create([
                        'ingredient_id' => $ingredient['ingredient_id'],
                        'measureunit_id' => $ingredient['measureunit_id'],
                        'amount' => $ingredient['amount']
                    ]);
                }
            }
        }

        return Recipe::find($recipe->id);
    }

}
