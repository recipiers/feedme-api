<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMeasureUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('measureunits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name',50)->default('???');
            $table->string('symbol',30)->default(' ');
            $table->unsignedBigInteger('measure_unit_type_id')->nullable();
            $table->unsignedDecimal('genericequivalent',12,3)->default(1.0);
            $table->foreign('measure_unit_type_id')
                    ->references('id')
                    ->on('measureunittypes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('measureunits');
    }
}
