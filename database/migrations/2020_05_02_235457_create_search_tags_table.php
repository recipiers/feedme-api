<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSearchTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('search_tag', function (Blueprint $table) {
            $table->unsignedBigInteger('search_id');
            $table->string('tagname',30);
            $table->boolean('included')->default(true);
            $table->foreign('search_id')->references('id')->on('user_searches');
            $table->primary(['search_id','tagname']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('search_tag');
    }
}
