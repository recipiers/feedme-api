<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIngredientSearchTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ingredient_search', function (Blueprint $table) {
            $table->unsignedBigInteger('search_id');
            $table->unsignedBigInteger('ingredient_id');
            // TODO: WHY THIS ?? IF ARE NOT INCLUDED WE JUST DELETE! AND IF NEEDED WE CREATE AGAIN!
            // David response:
            // - If included = true, then search results ONLY includes recipes CONTAINING the ingredient.
            // - If included = false, then search results ONLY includes recipes NOT CONTAINING the ingredient.
            // - If entry for a given ingredient doesn't exist, ingredient presence or absence are both ignored on search.
            $table->boolean('isincluded')->default(true);
            $table->primary(['search_id','ingredient_id']);
            $table->foreign('search_id')->references('id')->on('user_searches')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ingredient_search');
    }
}
