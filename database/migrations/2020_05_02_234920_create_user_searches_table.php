<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserSearchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_searches', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->string('title',127)->default('');
            $table->string('name',127)->nullable();
            $table->boolean('isvegetarian')->default(false);
            $table->boolean('isvegan')->default(false);
            $table->boolean('isprimarysearch');
            $table->integer('maxtime')->default(0);
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
            //$table->primary(['user_id','search_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_searches');
    }
}
