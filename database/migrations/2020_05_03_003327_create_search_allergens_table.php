<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSearchAllergensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('search_allergens', function (Blueprint $table) {
            $table->unsignedBigInteger('search_id');
            $table->unsignedBigInteger('allergen_id');
            $table->primary(['allergen_id','search_id']);
            $table->foreign('search_id')->references('id')->on('user_searches');
            $table->foreign('allergen_id')->references('id')->on('allergens');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('search_allergens');
    }
}
