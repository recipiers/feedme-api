# FEEDME-API

## ¿Qué es ?
Feedme es una aplicación creada por los alumnos de la Escola del Treball (Barcelona) y es **un buscador de recetas customizable, 100% libre e inclusivo**
## Básicos
Esta api ha sido creada ultilizando el framework [Laravel 6](https://laravel.com/docs/6.x/installation) y por esto tiene las siguientes dependencias iniciales


-    PHP >= 7.2.0
-    BCMath PHP extensión
-    Ctype PHP extensión
-    Fileinfo PHP extensión
-    JSON PHP extensión
-    Mbstring PHP extensión
-    OpenSSL PHP extensión
-    PDO PHP extensión
-    Tokenizer PHP extensión
-    XML PHP extensión
-    [Composer](https://getcomposer.org/)

### Configuración de un proyecto Laravel
#### Recomendamos seguir la documentación oficial de [Laravel 6](https://laravel.com/docs/6.x), un pequeño resumen es:
Descargue el proyecto: 
```
$ git clone https://gitlab.com/recipiers/feedme-api/
```
Entre en la carpeta del proyecto
```
$ cd feedme-api
```
Descargue las dependecias que vienen por composer 
```
composer install
```
Inicie y configure la base de datos en el archivo .env después de copiar el ejemplo
```
cp .env.example .env
```
Inicie el servidor
```
php artisan serve
```


### Listar todas las recetas
* Para respuestas que devuelvan muchas recetas a la vez: tener en cuenta que esto no devuelve toda las informaciones posibles de todas las recetas, como serían los ingredientes, los pasos, los alérgenos, etc. Pero sí la imagen, nombre , tiempo ... informaciones que serán señaladas al usuario después de hacer una búsqueda de informaciones para la tabla de recetas que es una previsualización de la receta. Para obtener la demás información se debe buscar la receta concreta de forma individual. 

### Request
- Método: `GET`

- Ruta (URL) : `/api/recipes`

### Ejemplo
```
GET /api/recipes
```

### Response
```json
[
  {
    "id": 1,
    "name": "Tortilla francesa",
    "description": "Receta simple de tortilla francesa",
    "time": 5,
    "pictureurl": "https:\/\/vdmedia.elpais.com\/elpaistop\/201711\/8\/201711081227871_1510141189_asset_still.png",
    "numportions": 1,
    "difficulty": 1
  },
  {
    "id": 2,
    "name": "Tomates secos",
    "description": "Super saludable y facil",
    "time": 30,
    "pictureurl": "http:\/\/cdn.elcocinerocasero.com\/imagen\/receta\/1000\/2015-11-11-19-02-41\/tomates-secos-en-aceite-de-oliva.jpeg",
    "numportions": 2,
    "difficulty": 3
  },
  {
    "id": 3,
    "name": "Tarta de fresas y crema pastelera",
    "description": "Esta una esas recetas de postre que es un claro ejemplo de que no hace falta ser un cocinillas pro para hacer una tarta deliciosa.\n\n            Una demostración práctica de que, en casa, sin muchos conocimientos de cocina, con sólo un poco de tiempo, podemos preparar postres y dulces de rechupete.\n\n            Esta tarta consta de dos preparaciones, a cual más sencilla. Por una lado una crema pastelera súper fácil  de preparar y muy rica.",
    "time": 60,
    "pictureurl": "https:\/\/www.recetasderechupete.com\/wp-content\/uploads\/2016\/07\/tarta_fresas_crema_pastelera-525x360.jpg",
    "numportions": 10,
    "difficulty": 4
  },
  ...
]
```


### Listar todos los ingredientes
* esto será ultilizado por el autocompletador (al hacer búsquedas por recetas), para ayudar al usuario y para traducir entre el nombre de los ingredientes y sus identificadores, dejando claro que el ingrediente es  exactamente aquél aprobado por el usuario
### Requisición (Request)
- Method: `GET`
- Ruta (URL): `/api/ingredients`
### Ejemplo
```html
GET /api/ingredients
```
#### Response
```json
{
  "status": 200,
  "success": true,
  "data": [
    {
      "id": 1,
      "name": "leche",
      "description": "leche de vaca",
      "isvegan": 0,
      "isvegetarian": 1
    },
    {
      "id": 2,
      "name": "huevos",
      "description": "Huevos de gallina, tamaño mediano",
      "isvegan": 0,
      "isvegetarian": 1
    },
    ...
}
```


## Obtener todos los alérgenos

### Request
- Methodo: `GET`
### Ruta (URL)

```
 /api/allergens
```

#### Ejemplo

```
GET /api/allergens
```

### Respuesta

```json
{
    "status": 200,
    "message": "OK",
    "allergens": [
         {
            "id": 1,
            "name": "lácteos",
            "description": "Este elemento contiene lactosa, con lo cual no está indicado para personas intolerantes a la lactosa."
        },
        {
            "id": 2,
            "name": "gluten",
            "description": "Este elemento contiene gluten, con lo cual no está indicado para personas celíacas."
        },
        {
            "id": 3,
            "name": "frutos secos",
            "description": "Este elemento contiene frutos secos, con lo cual no está indicado para personas intolerantes o alérgicas a los frutos secos."
        },
        [...]
    ] 
}

```


## ¿Cómo enviar datos a la api para parámetros de búsqueda ?
Recomendamos que se envíen los datos en formato json (apesar que otros formatos son soportados, json es el recomendable) para enviar en formato json se añade el Header apropriado
```
Content-Type: application/json
```
# Búsquedas
## Realizar una búsqueda customizada de recetas

Obtiene todas las recetas que satisfagan los criterios de búsqueda dados.

Si hay una sesión de usuario activa, además, se incluirá también información de si esas recetas están entre sus favoritas o no.

### Request

- Método: `POST`

### Ruta (URL)

```
/api/recipes/search
```

### Parámetros Body

Todos los parámetros siguientes son opcionales. Omitirlos todos da los mismos resultados que el endpoint de obtener todas las recetas.

- `name`: Los resultados de recetas se reducen a aquellos que poseen en su nombre el valor de este parámetro.

- `includedIngredients`: recetas que no contengan ninguno de estos ingredientes serán filtradas. Contiene los IDs de los ingredientes en cuestión.

- `excludedIngredients`: Lista de ingredientes que no incluirán las recetas buscadas. Contiene los IDs de los ingredientes en cuestión.

- `isvegetaria`: Si vale `true`, solamente incluirá en los resultados recetas vegetarianas o veganas. Si vale `false`, este parámetro es ignorado. Si `isvegan = true`, se ignora el valor de este parámetro.

- `isvegan`: Si vale `true`, solamente incluirá en los resultados recetas veganas. Si vale `false`, este parámetro es ignorado. Cuando es `true`, tiene prioridad sobre el valor de `isvegetaria`

- `maxtime`: Tiempo máximo de elaboración del cual se incluirán recetas en los resultados. Si vale `0` o se omite, se asume que no hay tiempo de elaboración máximo especificado y se ignora.

- `allergens`: Se excluyen todas las recetas que contengan alguno de los alérgenos indicados. Esto es, si al menos uno de los ingredientes de la receta contiene al menos uno de los alérgenos especificados, se descartará dicha receta. Contiene el nombre de los alérgenos en cuestión ejemplo "gluten" . Si se omite, no se descartarán recetas por alérgenos.

- `tags`: Solamente incluye en los resultados de la búsqueda aquellas recetas que contengan las tags especificadas.



### Ejemplo:

```json
POST /api/recipes/search
Content-Type: application/json

{
    "includedIngredients": [1, 2, 3],
    "excludedIngredients": [4],
    "isvegetaria": true,
    "isvegan": false,
    "maxtime": 25,
    "allergens": ["gluten"],
    "tags": ["salsa"]
}
```

### Respuesta:

```json
{
    "success": true,
    "status": 200,
    "search": {
        "includedIngredients": [1, 2, 3],
        "excludedIngredients": [4],
        "isvegetaria": true,
        "isvegan": false,
        "maxtime": 25,
        "allergens": ["gluten"],
        "tags": ["salsa"]
    },
    "recipes": [
        {
            "id": 25,
            "name": "Salsa pomodoro",
            "description": "Salsa de tomate de preparación rápida, utilizable para complementar pizzas y pasta",
            "time": 25,
            "pictureurl": "https://assets.bonappetit.com/photos/57ae32601b33404414975b70/16:9/w_2560,c_limit/quick-pomodoro-sauce-646.jpg",
            "numportions": 2,
            "difficulty": 2
        },
        {
        
            "id": 13,
            "name": "Salsa carbonara (versión vegetariana)",
            "description": "Receta de salsa carbonara apta para vegetarianos",
            "time": 20,
            "pictureurl": "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTcqk8X6JEmKr60RgmsghoOdeZ8YI5I84Felx5oBdUNCfWE1FaK&usqp=CAU",
            "numportions": 4,
            "difficulty": 2
        },
        [...]
    ]
}
```

# Register
* Algunas funcionabilidad necesitan del Authorization Token de un usuario para conseguirlo necesitas registrar un usuario y [logearlo](#login)
### Request
- Methodo: `POST`
- URL: `/api/register`
- Header: `Content-Type: application/json`

### Parametros 
- `name`: El nombre del usuario
- `email`: El mail del usuario, tiene de tener el formato de un email
- `password`: La contraseña para logear el usuario y conseguir su token
- `password_confirmation`: La confirmacion de la contraseña, tiene de ser igual a la contraseña para que se cree el usuario

### Ejemplo
```json
POST /api/register
Content-Type: application/json

{
	"name": "david",
	"email": "david@feedmail.cat",
	"password": "777777777",
	"password_confirmation": "777777777"
}
```
### Response
```json
{
  "success": true,
  "message": "Registro OK",
  "user": {
    "id": 12,
    "name": "david",
    "isAdmin": 0,
    "email": "david@feedmail.cat",
    "email_verified_at": null,
    "api_token": null,
    "remember_token": null,
    "created_at": "2020-06-13 15:44:36",
    "updated_at": "2020-06-13 15:44:36"
  }
}
```

### Login
- Requiere tener un [usuario ya registrado](####register) 
- Esta es la requisicion hecha para conseguir el <a name="Authorization" href="https://auth0.com/learn/token-based-Authorization-made-easy/">Authorization token</a> ultilizado para la autenticacion el los demas peticiones
### Request
- Método:`POST`
- Para obtener la token de usuario ponga el parametro `getToken`
### Ejemplo 
```json
POST /api/login
Content-Type: application/json

{
	"email": "david@feedmail.cat",
	"password": "777777777",
	"getToken": true
}
```
### Response
```json
{
  "success": true,
  "message": "Login OK",
  "status": 200,
  "user": {
    "sub": 12,
    "email": "david@feedmail.cat",
    "name": "david",
    "iat": 1592063080,
    "exp": 1592149480
  },
  "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyLCJlbWFpbCI6ImVkdWFyZEBub2VzdG95ZGVhY3VlcmRvLmVzbG9xdWVoYXkiLCJuYW1lIjoiZWR1YXJkIiwiaWF0IjoxNTkyMDYzMDgwLCJleHAiOjE1OTIxNDk0ODB9.SiILLxLFAw33xIgab2GF3W2S3Dw6-Z9ddx5ahgbsefM"
}
```


### Cerrar sesión de usuario

Elimina la sesión de usuario actual.

- Método: `POST`
- Requiere: Autenticación

```
/api/logout
```

#### Ejemplo

##### Petición URL

```
POST /api/logout
```

##### Respuesta

```json
{
    "status": 200,
    "message": "Sesión cerrada con éxito"
}
```

#### Errores

##### No autorizado

Si no existe una sesión de usuario activa en el momento de enviar una petición a este endpoint, se obtendrá un error `401` (`Unauthorized`).

```json
{
    "success": false,
    "status": 401,
    "message": "Se requiere una sesión de usuario activa para realizar esta operación" 
}
```

### Ver perfil de usuario actual

Obtiene los datos del usuario logueado actualmente en la aplicación.

Incluye recetas favoritas, búsquedas almacenadas, búsqueda primaria.

- Método: `GET`
- Requiere: Autenticación

```
/api/user/me
```

#### Ejemplo

##### Petición URL

```
GET /api/user/me
```

##### Respuesta

```json
{
  "success": true,
  "status": 200,
  "message": "OK",
  "user": {
    "id": 15,
    "name": "david",
    "isAdmin": 0,
    "email": "david@prueba.com",
  }
}
```

#### Errores

##### No autorizado

Si no existe una sesión de usuario activa en el momento de enviar una petición a este endpoint, se obtendrá un error `401` (`Unauthorized`).

```json
{
    "success": false,
    "status": 401,
    "message": "Se requiere una sesión de usuario activa para realizar esta operación" 
}
```

### Cambiar contraseña de usuario actual

Modifica el valor de la contraseña de usuario actual

- Método: `POST`
- Requiere: Autenticación

```
/api/user/changepass
```

#### Parámetros POST:

- oldpassword: Contraseña antigua.
- newpassword: Contraseña nueva.
- newpassword_confirmation: Debe coincidir con la contraseña nueva.

### Eliminar cuenta de usuario

Elimina la cuenta del usuario de la sesión actual.

Es irreversible y eliminará también todas las asociaciones de recetas favoritas y de búsquedas almacenadas del usuario. 

Se recomienda hacer consciente al usuario de la aplicación de toda esta información antes de que acceda a realizar esta operación.

- Método: `DELETE`
- Requiere: Autenticación

```
/api/user
```

#### Ejemplo

##### Petición URL

```
DELETE /api/user
```

##### Respuesta

```json
{
    "status": 200,
    "success": true,
    "message": "Cuenta de usuario eliminada con éxito"
}
```

#### Errores

##### No autorizado

Puesto que esta acción se realiza sobre el mismo usuario de la sesión, es necesario que exista una sesión de usuario activa para realizar la operación.

En caso de que dicha sesión no esté activa, se devolverá un error con la siguiente estructura:

```json
{
    "success": false,
    "status": 401,
    "message": "Se requiere una sesión de usuario para realizar esta acción"
}
```


## Busquedas Salvas
- Requiere [Autenticacion](#Login)
- Las busquedas salvas se dividen en dos tipos, la busqueda primaria y las demas busquedas
- La busqueda primaria esta pensada para gente con intolerancias, vegetarianos o otras dietas, alérgicos ... en esta estaran los filtros predefinidos, pero puedes cambiarlos cuando quieras

## Salvar Busqueda Primaria
### Request
- Metodo: `PUT`
- El json tendra el formato de una [busqueda](#Busqueda)
- URL : `/api/user/primarySearch`

### Ejemplo
```json
PUT /api/user/primarySearch
Authorization: <TU TOKEN AQUI>
Content-Type: Application/json

{
    "isvegetaria": true,
    "maxtime": 25,
    "allergens": ["gluten"]
}
```



## ERRORES:

#### No autorizado

Si no existe una sesión de usuario activa en el momento de enviar una petición a alguno de los endpoints que requiere un usuario logeado como los de salvar busquedas, se obtendrá un error `401` (`Unauthorized`).

```json
{
    "success": false,
    "status": 401,
    "message": "Se requiere una sesión de usuario activa para realizar esta operación" 
}
```

## Mirar tu Busqueda Primaria
### Request
- Requiere: [Autenticacion](#login)
- Método: `GET`
- URL : `/api/user/primarySearch`

### Ejemplo
```http
GET /api/user/primarySearch
Authorization: <TU TOKEN AQUI>
```


### Response
```json
{
  "success": true,
  "primarySearch": {
    "id": 6,
    "title": "",
    "name": "",
    "isvegetarian": 0,
    "isvegan": 0,
    "maxtime": 0,
    "created_at": null,
    "updated_at": null,
    "excludedIngredients": [
      {
        "id": 4,
        "name": "chocolate con leche",
        "description": "Chocolate oscuro con leche",
        "isvegan": 0,
        "isvegetarian": 1,
        "pivot": {
          "search_id": 6,
          "ingredient_id": 4
        }
      }
    ],
    "allergens": [
      {
        "id": 2,
        "name": "gluten",
        "description": "Este elemento contiene gluten, con lo cual no está indicado para personas celíacas.",
        "created_at": null,
        "updated_at": null,
        "pivot": {
          "search_id": 6,
          "allergen_id": 2
        }
      }
    ]
  }
}
```

## Salvar Busquedas comunes
- Una busqueda comun o sea, todas las busquedas salvas aparte de la primaria son muy parecida con la primaria con las siguientes distinciones
- Tienen un titulo, por exemplo "Dulces vegetarianos"
- No son aplicadas automaticamente, tendras una lista de tus busquedas salvas y puedes elegir entre ellas cuando quieras

### Request
- Requiere: [Autenticacion](#login)
- Metodo: `POST`
- El json tendra el formato de una [busqueda](#Busqueda) ademas de un titulo
- URL : `/api/user/storedSearches`

### Ejemplo
```http
POST /api/user/storedSearches
Authorization: <TU TOKEN AQUI>
Content-Type: Application/json

{
    "title": "Fast vegetarians",
    "isvegetaria": true,
    "maxtime": 35
}
```

### Response

```
{
  "success": true,
  "message": "Búsqueda almacenada con éxito"
}
```


### Obtener búsquedas guardadas del usuario logueado

Obtiene la lista de búsquedas guardadas por el usuario de los parámetros de búsqueda especificados en la búsqueda por defecto del usuario.

Todos los usuarios tienen una búsqueda por defecto, que especifica los valores predeterminados para la realización de cualquier búsqueda.

El usuario del cual se obtienen las búsquedas es el usuario que esté logueado a la API mediante el header `Authorization`.

Las búsquedas almacenadas tendrán un título humanamente legible para que el usuario las pueda identificar y diferenciar del resto.

- Ruta (URL) : `/api/user/storedSearches`
- Necesita del `Authorization` token de tu login 

#### EJEMPLO

```
GET /api/user/storedSearches
Authorization : <USER_TOKEN>

```

##### Respuesta

```json
{
    "status": 200,
    "success": true,
    "message": "OK",
    "storedSearches": [
        {
            "id": 17,
            "title": "Salsas vegetarianas con tomate",
            "name": "tomate",
            "includedIngredients": [7],
            "excludedIngredients": [],
            "isvegetaria": true,
            "isvegan": false,
            "maxtime": 0,
            "allergens": [],
            "tags": ["salsa"] 
        },
        {
            "id": 25,
            "title": "Ensaladas rápidas de hacer sin cebolla",
            "name": "ensalada",
            "includedIngredients": [],
            "excludedIngredients": [1],
            "isvegetaria": false,
            "isvegan": false,
            "maxtime": 20,
            "allergens": [],
            "tags": ["ensalada"]
        },
        {
            "id": 32,
            "title": "Comida para perros",
            "includedIngredients": [],
            "excludedIngredients": [],
            "isvegetaria": false,
            "isvegan": false,
            "maxtime": 0,
            "allergens": [],
            "tags": ["comida-de-perro"]
        }
    ]
}
```

### Actualizar búsqueda primaria del usuario logueado

Modifica los criterios de búsqueda de la búsqueda por defecto, es decir, los valores por defecto que se establecerán al realizar una búsqueda, del usuario logueado actualmente en la API.

La búsqueda primaria no necesita título.

- Método: `PUT`
- Requiere: `Autenticación`
- Ruta (URL) : `/api/user/primarySearch`

#### EJEMPLO


```json
PUT /api/user/primarySearch
Authorization: <USER_TOKEN>
Content-Type: application/json

{
    "includedIngredients": [],
    "excludedIngredients": [],
    "isvegetaria": false,
    "isvegan": true,
    "maxtime": 75,
    "allergens": [],
    "tags": []
}
```

#### Respuesta

```json
{
    "status": 200,
    "success": true,
    "message": "Búsqueda primaria actualizada con éxito",
    "oldPrimarySearch": {
        "includedIngredients": [],
        "excludedIngredients": [],
        "isvegetaria": true,
        "isvegan": false,
        "maxtime": 50,
        "allergens": [],
        "tags": []
    },
    "newPrimarySearch": {
        "includedIngredients": [],
        "excludedIngredients": [],
        "isvegetaria": false,
        "isvegan": true,
        "maxtime": 75,
        "allergens": [],
        "tags": []   
    }
}
```

### Actualizar búsqueda guardada del usuario logueado

Modifica los criterios de búsqueda de una de las búsquedas almacenadas de usuario logueado actualmente en la API.

Método: `PUT`

```
/api/user/storedSearches/${id}
```

#### Parámetros GET

- `id`: El identificador de la búsqueda guardada a editar. Tener en cuenta que la búsqueda debe corresponder al usuario que realiza la petición.

#### Parámetros POST

Todos los parámetros son opcionales.

- `title`: El título con el que el usuario identifica la búsqueda almacenada. Se recomienda que un usuario no tenga almacenadas dos búsquedas con el mismo título.

- `name`: parámetro de búsqueda por nombre. Todas las recetas dadas por la búsqueda contendrán en el nombre el valor de este atributo.

- `includedIngredients`: Lista de ingredientes que incluirán las recetas buscadas. Contiene los IDs de los ingredientes en cuestión.

- `excludedIngredients`: Lista de ingredientes que no incluirán las recetas buscadas. Contiene los IDs se los ingredientes en cuestión.

- `isvegetaria`: Si vale `true`, solamente incluirá en los resultados recetas vegetarianas o veganas. Si vale `false`, este parámetro es ignorado. Si `isvegan = true`, se ignora el valor de este parámetro.

- `isvegan`: Si vale `true`, solamente incluirá en los resultados recetas veganas. Si vale `false`, este parámetro es ignorado. Cuando es `true`, tiene prioridad sobre el valor de `isvegetaria`

- `maxtime`: Tiempo máximo de elaboración del cual se incluirán recetas en los resultados. Si vale `0` o se omite, se asume que no hay tiempo de elaboración máximo especificado y se ignora.

- `allergens`: Se excluyen todas las recetas que contengan alguno de los alérgenos indicados. Esto es, si al menos uno de los ingredientes de la receta contiene al menos uno de los alérgenos especificados, se descartará dicha receta. Contiene los IDs de los alérgenos en cuestión. Si se omite, no se descartarán recetas por alérgenos.

- `tags`: Solamente incluye en los resultados de la búsqueda aquellas recetas que contengan las tags especificadas.

#### Ejemplo

#### Petición: URL

```
PUT /api/user/storedSearches/137
```

#### Parámetros BODY

```json
{
    "title": "Desayunos",
    "includedIngredients": [],
    "excludedIngredients": [],
    "isvegetaria": false,
    "isvegan": false,
    "maxtime": 20,
    "allergens": [],
    "tags": ["desayuno"]
}
```

#### Respuesta

```json
{
    "success": true,
    "status": 200,
    "message": "Búsqueda almacenada modificada con éxito",
    "oldSearch": {
        "title": "Mañana",
        "includedIngredients": [1, 3],
        "excludedIngredients": [2],
        "isvegetaria": false,
        "isvegan": false,
        "maxtime": 30,
        "allergens": [],
        "tags": []
    },
    "newSearch": {
        "title": "Desayunos",
        "includedIngredients": [],
        "excludedIngredients": [],
        "isvegetaria": false,
        "isvegan": false,
        "maxtime": 20,
        "allergens": [],
        "tags": ["desayuno"]
    },
    
}

```
### Errores

#### Autenticacion (Authorization Header ) 
[Lea sobre como hacer login e conseguir tu token de autenticacion](#login)
#### Búsqueda no encontrada

Si la búsqueda correspondiente al ID enviado no existe, se devolverá una respuesta de error con este formato:

```json
{
    "success": false,
    "status": 404,
    "message": "No hay ninguna búsqueda almacenada asociada a ese ID."
}
```

### Guardar nueva búsqueda personalizada de usuario

Crea una nueva búsqueda almacenada de usuario.

Un usuario podrá almacenar un máximo de 3 búsquedas personalizadas.

- Método: `POST`
- Parametros de [busqueda](#busqueda) + titulo (para la distiguir la busqueda salva)
- Requiere: [Autenticación](#login) (Authorization Header)
- Ruta (URL) : `/api/user/storedSearches`


#### Ejemplo

```json
POST /api/user/storedSearches
Authorization: <USER_TOKEN>
Content-Type: application/json

{
    "title": "Desayunos",
    "includedIngredients": [],
    "excludedIngredients": [],
    "isvegetaria": false,
    "isvegan": true,
    "maxtime": 20,
    "allergens": [],
    "tags": ["desayuno"]
}
```

#### Respuesta

```json
{
    "success": true,
    "status": 200,
    "message": "Búsqueda almacenada insertada con éxito",
    "search": {
        "title": "Desayunos",
        "includedIngredients": [],
        "excludedIngredients": [],
        "isvegetaria": false,
        "isvegan": false,
        "maxtime": 20,
        "allergens": [],
        "tags": ["desayuno"]
    }
}
```

#### Errores

##### No autorizado

Si no existe una sesión de usuario activa en el momento de enviar una petición a este endpoint, se obtendrá un error `401` (`Unauthorized`).
- lea mas sobre [autenticacion](#login)

```json
{
    "success": false,
    "status": 401,
    "message": "Se requiere una sesión de usuario activa para realizar esta operación" 
}
```

#### Máximo de búsquedas guardadas alcanzado

Un usuario tiene un límite de almacenamiento de 3 búsquedas. 

Si se intenta almacenar otra búsqueda habiéndose alcanzado ese límite, se enviará una respuesta de error con el siguiente formato:

```json
{
    "success": false,
    "status": 200,
    "message": "Un usuario no puede tener más de tres búsquedas almacenadas."
}
```

### Borrar búsqueda personalizada de usuario

Elimina una de las búsquedas de usuario almacenadas.

La búsqueda a borrar debe pertenecer al usuario que la elimina.

- Método: `DELETE`
- Requiere: Autenticación
- Ruta (URL) : `/api/user/storedSearches/{id}`

#### Parámetros GET

- id: ID de la búsqueda a eliminar.

#### Ejemplo

```
DELETE /api/user/storedSearches/32
Authorization: <USER_TOKEN>
```

##### Respuesta

```json
{
    "success": true,
    "message": "Búsqueda borrada con éxito",
    "search": {
        "id": 32,
        "title": "Comida para perros",
        "includedIngredients": [],
        "excludedIngredients": [],
        "isvegetaria": false,
        "isvegan": false,
        "maxtime": 0,
        "allergens": [],
        "tags": ["comida-de-perro"]
    }
}
```
#### Errores

##### No autorizado

Si no existe una sesión de usuario activa en el momento de enviar una petición a este endpoint, se obtendrá un error `401` (`Unauthorized`).

```json
{
    "success": false,
    "status": 401,
    "message": "Se requiere una sesión de usuario activa para realizar esta operación" 
}
```

##### Búsqueda no encontrada

En caso de que no exista ninguna búsqueda almacenada con el ID indicado, se devolverá una respuesta de error con este formato:

```json
{
    "success": false,
    "status": 404,
    "message": "No se ha encontrado la búsqueda con el ID especificado" 
}
```

### Ver receta 
TODO : Poner esto despues de la parte de listar todas las recetas, antes de login y rutas de autenticacion

Obtiene la información detallada relativa a la receta indicada. 

También obtiene su lista de ingredientes con cantidades, sus alérgenos, etiquetas y si se encuentra entre las recetas favoritas del usuario en caso de haber un usuario logueado en la aplicación.

- Método: `GET`

- Ruta (URL) : `/api/recipes/{id}`

#### Parámetros GET:

- `id`: Id de la receta buscada.

#### Ejemplo

##### Petición URL

```
GET /api/recipes/13
```

##### Respuesta

```json
{
    "status": 200,
    "success": true,
    "message": "OK",
    "recipe": {
        "id": 1,
        "name": "Tortilla francesa",
        "description": "Receta sencilla de tortilla francesa",
        "time": 5,
        "pictureurl": "https:\/\/vdmedia.elpais.com\/elpaistop\/201711\/8\/201711081227871_1510141189_asset_still.png",
        "numportions": 1,
        "difficulty": 1,
        "steps": [
            "Poner una sartén en el fuego y echarle un chorrito de aceite",
            "Una vez el aceite se ha calentado, echarle un huevo.",
            "Remover los huevos y esperar a que cuajen. Se puede dejar más o menos tiempo, en función del nivel de cocción deseado."
        ],
        "ingredients": [
            {
                "ingredient": {
                    "id": 2,
                    "name": "huevos",
                    "description": "Huevos de gallina, tamaño mediano",
                    "isvegan": 0,
                    "isvegetaria": 1
                },
                "amount": 1.000,
                "unit": {
                    "id": 14,
                    "name": "unidad(es)",
                    "symbol": "u.",
                    "measure_unit_type_id": 3,
                    "genericequivalent": "1.000"
                }
                }
            ],
            "tags": []
        } 
    }
}
```

#### ERRORES

##### Receta no encontrada

En caso de no existir ninguna receta con el ID especificado, se devolverá una respuesta de error con el siguiente formato:

```json
{
    "success": false,
    "status": 404,
    "message": "No se ha encontrado la receta con el ID especificado" 
}
```

### Marcar/desmarcar receta como favorita

Marca una receta como favorita del usuario.

En caso de que la receta ya esté marcada como favorita, dejará de estarlo.

- Método: `POST`
- Requiere: Autenticación

```
/api/user/favrecipes/${id}
```

#### Parámetros GET

- `id`: Identificador de la receta que se desea marcar/desmarcar de favoritos para el usuario actual.

#### Ejemplo

##### Petición URL

```
POST /api/user/favrecipes/13
```

##### Respuesta

```json
{
    "status": 200,
    "success": true,
    "message": "Receta marcada como favorita",
    "recipe": {
        "id": 13,
        "name": "Fresas con nata",
        "time": 5,
        "pictureurl": null,
        "numportions": 1,
        "difficulty": 1,
        "isuserfav": true
    }
}
```

Y si realiza la misma petición de nuevo...


```json
{
    "status": 200,
    "success": true,
    "message": "Receta desmarcada de favoritas",
    "recipe": {
        "id": 13,
        "name": "Fresas con nata",
        "time": 5,
        "pictureurl": null,
        "numportions": 1,
        "difficulty": 1,
        "isuserfav": false
    }
}
```

#### Errores

##### No autorizado

Si no existe una sesión de usuario activa en el momento de enviar una petición a este endpoint, se obtendrá un error `401` (`Unauthorized`).

```json
{
    "success": false,
    "status": 401,
    "message": "Se requiere una sesión de usuario activa para realizar esta operación" 
}
```

---
### Obtener recetas favoritas del usuario logueado

Obtiene la lista de recetas favoritas del usuario.

El usuario del cual se obtienen las recetas favoritas es el usuario que esté logueado a la API mediante el header `Authorization`.

- Método: `GET`

```
/api/user/favrecipes
```

#### EJEMPLO

##### Petición
```
GET /api/user/favrecipes
Authorization: <USER_TOKEN>
```

##### Respuesta

```json
{
    "status": 200,
    "success": true,
    "message": "OK",
    "recipes": [
        {
        
            "id": 14,
            "name": "Paella valenciana",
            "description": "Paella valenciana de la de verdad, mixta de toda la vida.",
            "time": 200,
            "pictureurl": "https://www.comedera.com/wp-content/uploads/2014/02/paella-de-mariscos-700x400.jpg",
            "numportions": 6,
            "difficulty": 5
    ]
}
```



---



