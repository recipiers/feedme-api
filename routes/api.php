<?php

use App\Http\Controllers\SearchController;
use App\Ingredient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Model\Tag;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
Route::middleware('auth:api')
    ->get('/user', function(Request $request) {
        // Accede al usuario actual de la aplicación
        return $request->user();
    });
*/

/**
 * Aquí dentro van todas las rutas que requieren autenticación.
 */
Route::middleware('auth')->group(function() {

    // Usuario: General
    Route::get('user/me', 'UserController@getCurrentUser');
    Route::post('logout', 'Auth\LoginController@logout');
    Route::delete('user', "UserController@deleteCurrentUser");
    Route::put('user/changepass', 'UserController@changePassword');

    // Usuario: Recetas favoritas
    Route::get('user/favrecipes', 'UserController@getCurrentUserFavouriteRecipes');
    Route::post('user/favrecipes/{id}', 'UserController@toggleRecipeAsFavourite');
    Route::get('user/savedSearches','UserController@savedSearches');

    // Usuario: búsquedas
    Route::get('user/primarySearch', 'UserController@getPrimarySearch');
    Route::get('user/storedSearches', 'UserController@getStoredSearches');
    Route::post('user/storedSearches', 'UserController@storeNewSearch');
    Route::get('user/storedSearches/{id}', 'UserController@getStoredSearch');
    Route::put('user/storedSearches/{id}', 'UserController@updateStoredSearch');
    Route::delete('user/storedSearches/{id}', 'UserController@deleteStoredSearch');
    Route::put('user/primarySearch', 'UserController@updatePrimarySearch');

    Route::get('user/search/{id}/perform', 'SearchController@perform');
});


Route::post('login', 'Auth\LoginController@login');
Route::post('register', 'Auth\RegisterController@register');

Route::post('recipes/search','RecipeController@search');

Route::resource('recipes','RecipeController')->only([
    'index','show','store','update','destroy'
]);

Route::resource('ingredients', 'IngredientController')->only([
    'index','create','store','update','delete'
]);

Route::resource('allergens', 'AllergenController')->only([
    'index','create','store','update','delete'
]);

Route::get('tags',function () {
    return Tag::select('tagname')->distinct()->get()->pluck('tagname')->toArray();
});

Route::get('measureUnits', 'MeasureUnitController@index');

