DROP DATABASE IF EXISTS feedme;
CREATE DATABASE feedme;

\c feedme;

CREATE TABLE IF NOT EXISTS Recipes(
	id SERIAL PRIMARY KEY,
	name varchar(64) NOT NULL,
	description varchar(255) NOT NULL,
	time int,
	pictureurl varchar(255) NOT NULL,
	numPortions int NOT NULL,
	difficulty int NOT NULL
);

CREATE TABLE IF NOT EXISTS Ingredients(
	id SERIAL PRIMARY KEY,
	name varchar(100) NOT NULL,
	description text,
	isVegan boolean NOT NULL,
	isVegetarian boolean NOT NULL
);

CREATE TABLE IF NOT EXISTS Steps(
	step_id SERIAL PRIMARY KEY,
	recipe_id int REFERENCES Recipes (id),
	ordernum int NOT NULL,
	description varchar(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS Users(
	id SERIAL PRIMARY KEY,
	name varchar(255) NOT NULL,
	email varchar(255) UNIQUE NOT NULL,
	password varchar(255) NOT NULL,
	api_key varchar(64) UNIQUE NOT NULL
);

CREATE TABLE IF NOT EXISTS User_Searches(
	user_id int,
	search_id int,
	isvegetarian boolean NOT NULL,
	isvegan boolean NOT NULL,
	isprimarysearch boolean,
	maxTime numeric,
	FOREIGN KEY (user_id) REFERENCES Users (id),
	PRIMARY KEY (user_id,search_id)
);
-- Cambie un punto del diagrama
--  una recipe_tag no hace referencia a search_tag del usuarios
--  pero si un usuario tiene una search que tiene search tags que hace referencia a recipe_tags

CREATE TABLE IF NOT EXISTS Search_Tags(
	user_id int,
	search_id int,
	tagname varchar(20),
	included boolean NOT NULL default TRUE,
	FOREIGN KEY (user_id,search_id) REFERENCES User_Searches(user_id,search_id),
	PRIMARY KEY (user_id,search_id,tagname)
);

CREATE TABLE IF NOT EXISTS Recipe_Tags(
	recipe_id int,
	tagname varchar(20),
    FOREIGN KEY (recipe_id) REFERENCES Recipes (id)
	PRIMARY KEY (recipe_id, tagname)
);

CREATE TABLE IF NOT EXISTS MeasureUnitTypes(
	id SERIAL PRIMARY KEY,
	name varchar(50) UNIQUE NOT NULL,
	genericunit_id int NOT NULL
);

CREATE TABLE IF NOT EXISTS MeasureUnits(
	id SERIAL PRIMARY KEY,
	name varchar(50) NOT NULL,
	symbol varchar(20),
	measureunittype_id int REFERENCES MeasureUnitTypes,
	genericequivalent numeric
);

CREATE TABLE Recipe_Ingredient_Measures(
	recipe_id int REFERENCES Recipes (id) ,
	ingredient_id int REFERENCES Ingredients (id),
	measureunit_id int REFERENCES MeasureUnits (id),
	amount numeric,
	PRIMARY KEY (recipe_id, ingredient_id)
);

CREATE TABLE IF NOT EXISTS Ingredient_MeasureUnitTypes(
	ingredient_id int REFERENCES Ingredients (id) ,
	measureunittype_id int REFERENCES MeasureUnittypes (id) ,
	PRIMARY KEY (ingredient_id, measureunittype_id)
);

CREATE TABLE IF NOT EXISTS Allergens(
	id SERIAL PRIMARY KEY,
	name varchar(50) NOT NULL,
	description varchar(255)
);

CREATE TABLE IF NOT EXISTS Search_Allergens(
	user_id int,
	search_id int,
	allergen_id int REFERENCES Allergens (id),
	PRIMARY KEY (user_id,search_id),
	FOREIGN KEY (user_id,search_id) REFERENCES User_Searches,
	FOREIGN KEY (allergen_id) REFERENCES Allergens (id)
);

CREATE TABLE IF NOT EXISTS Allergen_Ingredient(
	ingredient_id int REFERENCES Ingredients (id),
	allergen_id int REFERENCES Allergens (id),
	PRIMARY KEY (ingredient_id, allergen_id)
);

CREATE TABLE IF NOT EXISTS Ingredient_Search(
	user_id int,
	search_id int,
	ingredient_id int REFERENCES Ingredients (id),
	isincluded boolean DEFAULT TRUE,
	FOREIGN KEY (user_id, search_id) REFERENCES User_Searches (user_id, search_id),
	PRIMARY KEY (user_id, search_id, ingredient_id)
);

CREATE TABLE IF NOT EXISTS User_FavRecipes(
	user_id int REFERENCES Users (id),
	recipe_id int REFERENCES Recipes (id)
);

