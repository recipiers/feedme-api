<?php

namespace App\Helpers;

use App\Model\User;
use Firebase\JWT\JWT;

class JwtAuth
{
    /**
     * Tiempo de vida de las tokens generadas en segundos.
     */
    const TOKEN_LIFETIME_S = 60 * 60 * 24;

    /**
     * Clave privada utilizada para generar la token.
     */
    private $secretKey;

    /**
     * Algoritmo utilizado para cifrar la información.
     */
    private $cipherAlgorithm;

    public function __construct() {
        $this->secretKey = 'esta-es-la-clave-secreta-de-tokens';
        $this->cipherAlgorithm = 'HS256';
    }

    /**
     * @param $email User email
     * @param $passwordHash Password hash
     */
    public function signup($email, $passwordHash, $getToken = null)
    {
        // Se busca un usuario que coincida con el email y la contraseña dados
        $user = User::where([
            "email" => $email,
            "password" => $passwordHash
        ])->first();

        // Si se obtiene dicho usuario, se genera la token.
        if (is_object($user)) {
            // Generar información del token
            $token = [
                'sub' => $user->id,
                'email' => $user->email,
                'name' => $user->name,
                'iat' => time(), // Momento de creación del token
                'exp' => time() + JwtAuth::TOKEN_LIFETIME_S // Momento de caducidad del token
            ];
            // Generar token codificada
            $jwt = JWT::encode($token, $this->secretKey, $this->cipherAlgorithm);
            // generar versión decodificada de la token
            $decodedJWT = JWT::decode($jwt, $this->secretKey, [$this->cipherAlgorithm]);

            // En función del valor de getToken, se devuelve la token codificada o decodificada.
            if (!is_null($getToken)) {
                return $jwt;
            } else {
                return $decodedJWT;
            }
        } else {
            // En caso de no obtener un usuario con las credenciales dadas, se devuelve un error.
            return null;
        }
    }

    /**
     * Comprueba si una token dada es correcta.
     *
     * @param $jwt string La token JWT dada.
     * @param $getIdentity boolean Determina si se desea obtener la token descodificada en caso de ser la token correcta.
     */
    public function checkToken($jwt, $getIdentity = false)
    {
        // Inicializamos variables
        $auth = false;
        $decoded = null;

        try {
            // Intentamos decodificar la token.
            // Si la decodificación fallase, se considera que la token no es correcta o está caducada.
            $decoded = JWT::decode($jwt, $this->secretKey, [$this->cipherAlgorithm]);
        } catch (\UnexpectedValueException $e) {
            return false;
        } catch (\DomainException $e) {
            return false;
        }

        if ($getIdentity) {
            // Si getIdentity se ha enviado como true, se devuelve la token decodificada
            // NOTA: Si la token no se ha podido decodificar, $decoded valdrá null.
            return $decoded;
        } else {
            // Si getIdentity no es true, se devuelve true o false en función de si se puede
            // decodificar la token y si ésta tiene una identidad.
            return (isset($decoded) && !is_null($decoded) && isset($decoded->sub));
        }
    }

}
