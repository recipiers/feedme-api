<?php

namespace App\Repositories;

use App\Model\Recipe;
use App\Model\RecipeIngredientMeasure;
use App\Model\Step;
use App\Model\Tag;

use App\Repositories\Interfaces\RecipeRepositoryInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use Illuminate\Support\Str;

use App\Http\Requests\SearchRequest;

class RecipeRepository implements RecipeRepositoryInterface
{

    protected $model;

    public function __construct(Recipe $recipe)
    {
        $this->model = $recipe;
    }

    /**
     * Obtiene todas las recetas.
     */
    public function all()
    {
        $result = $this->model->all();
        return $result;
    }

    /**
     * Crea una nueva receta a partir de los datos otorgados.
     */
    public function create(array $data)
    {
        $recipe = new Recipe();
        $recipe->name = $data['name'];
        $recipe->time = $data['time'];
        $recipe->difficulty = $data['difficulty'];
        $recipe->description = $data['description'];
        $recipe->pictureurl = $data['pictureurl'];
        $recipe->numportions = $data['numportions'];
        $recipe->save();
        foreach ($data['steps'] as $key => $value) {
            $recipe->steps()->save(new Step([
                'ordernum' => $key,
                'description' => $value
            ]));
        }

        foreach ($data['ingredients'] as $ingredient) {
            // Todo check if this is being validated
            $ingredientsRecipe = $recipe->ingredients()->pluck('ingredient_id')->toArray();
            if (!in_array($ingredient['ingredient_id'], $ingredientsRecipe)) {
                $recipe->ingredients()->create([
                    'ingredient_id' => $ingredient['ingredient_id'],
                    'measureunit_id' => $ingredient['measureunit_id'],
                    'amount' => $ingredient['amount']
                ]);
            }
        }
        /*$recipe['ingredients'] = $recipe->ingredients()->get()->toArray();
        $recipe['steps'] = $recipe->steps()->orderBy('ordernum')->pluck('description')->toArray();
        */
        return $this->find($recipe->id);
    }

    /**
     * Actualiza los datos de una receta.
     */
    public function update(array $data, $id)
    {
        $recipe = $this->model
            ->findOrFail($id);
        $recipe->update($data);

        // Refrescar pasos de receta
        $recipe->steps()->delete();
        foreach ($data['steps'] as $step) {
            $recipe->steps()->save(new Step($step));
        }

        // TODO: insertar relación de ingredientes

        return $recipe;
    }

    /**
     * Elimina la receta con un ID dado.
     */
    public function delete($id)
    {
        return $this->model->destroy($id);
    }

    /**
     * Devuelve la receta correspondiente a un ID dado.
     */
    public function find($id)
    {
        $recipe = $this->model::with('steps', 'ingredients', 'tags')->findOrFail($id);

        $steps = array_map(function ($step) {
            return $step['description'];
        }, $recipe->steps()->get()->toArray());

        $ingredients = array_map(function ($ingredient) {
            return [
                'ingredient' => \App\Model\Ingredient::find($ingredient['ingredient_id']),
                'amount' => $ingredient['amount'],
                'unit' => \App\Model\MeasureUnit::find($ingredient['measureunit_id']),

            ];
        }, $recipe->ingredients()->get()->toArray());


        return [
            'id' => $recipe->id,
            'name' => $recipe->name,
            'description' => $recipe->description,
            'time' => $recipe->time,
            'pictureurl' => $recipe->pictureurl,
            'numportions' => $recipe->numportions,
            'difficulty' => $recipe->difficulty,
            'steps' => $steps,
            'ingredients' => $ingredients,
            'tags' => Tag::all()->where('recipe_id', $id)
        ];
    }

    /**
     * Devuelve todas las recetas vegetarianas.
     * Las recetas veganas son incluidas también por extensión.
     */
    public function vegetarian()
    {
        return $this->model->filter(function ($recipe) {
            return $recipe->isVegetarian();
        });
    }

    /**
     * Devuelve todas las recetas veganas.
     */
    public function vegan()
    {
        return $this->model->filter(function ($recipe) {
            return $recipe->isVegan();
        });
    }

    /**
     * Devuelve todas las recetas que no superan un tiempo de elaboración dado.
     */
    public function maxTime(int $minutes)
    {
        return $this->model::where('time', '<=', $minutes);
    }

    /**
     * Devuelve todas las recetas cuyo nombre contiene el nombre buscado.
     */
    public function searchByName(string $name)
    {
        return $this->model::whereLike('name', "'%{$name}%'");
    }

    /**
     * Devuelve recetas que satisfagan los requisitos de una búsqueda
     */
    public function search(SearchRequest $request)
    {
        $results = $this->model::all();

        if ($request->name) {
            $results = $results->reject(function ($rec) use ($request) {
                return !Str::contains(strtolower($rec->name), strtolower($request->name));
            });
        }

        if ($request->includedIngredients) {
            $results = $results->reject(function ($rec) use ($request) {
                $ingredients = $rec->ingredients()->pluck('ingredient_id')->toArray();

                foreach ($request->includedIngredients as $includedIngredient) {
                    if (!in_array($includedIngredient, $ingredients)) {
                        return true;
                    }
                }
            });
        }

        if ($request->excludedIngredients) {
            $results = $results->reject(function ($rec) use ($request) {
                $ingredients = $rec->ingredients()->pluck('ingredient_id')->toArray();

                foreach ($request->excludedIngredients as $excludedIngredient) {
                    if (in_array($excludedIngredient, $ingredients)) {
                        return true;
                    }
                }
            });
        }

        if ($request->maxtime) {
            $results = $results->reject(function (Recipe $recipe) use ($request) {
                return $recipe->time > $request->maxtime;
            });
        }

        if ($request->isvegetarian) {
            $results = $results->filter(function (Recipe $recipe) {
                return $recipe->isVegetarian();
            });
        }

        if ($request->isvegan) {
            $results = $results->filter(function (Recipe $recipe) {
                return $recipe->isVegan();
            });
        }

        if ($request->allergens) {
            $results = $results->filter(function (Recipe $recipe) use ($request) {
                // Gets all the allergens for this recipe
                $allergens = array_merge(... $recipe->ingredients()->get()->map( function($ing) { return $ing->ingredient()->first()->allergens()->pluck('name')->toArray(); })->toArray());
                foreach ($request->allergens as $allergen) {
                    if ( ! in_array($allergen, $allergens)) {
                        return true;
                    }
                }
                return false;
            });
        }

        if ($request->includedTags) {
            foreach ($request->includedTags as $tag) {
                $results = $results->filter(function (Recipe $recipe) use ($tag) {
                    $tags = $recipe->tags()->pluck('tagname')->toArray();
                    return in_array($tag, $tags, true);
                });
            }
        }

        return array_values($results->toArray());
    }
}
