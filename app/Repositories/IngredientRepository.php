<?php

namespace App\Repositories;

use App\Model\Ingredient;
use App\Repositories\Interfaces\IngredientRepositoryInterface;

class IngredientRepository implements IngredientRepositoryInterface {

    protected $model;

    public function __construct(Ingredient $ingredient)
    {
        $this->model = $ingredient->with('allergens');
    }

    /**
     * Obtiene todos los ingredientes vegetarianos.
     */
    public function vegetarian() {
        return $this->model
                ->where('vegan', true)
                ->orwhere('vegetarian', true);
    }

    /**
     * Obtiene todos los ingredientes veganos.
     */
    public function vegan() {
        return $this->model->where('vegan', true);
    }

    /**
     * Obtiene todos los ingredientes con un alérgeno determinado.
     */
    public function withAllergen(int $allergenId) {
        return $this->model->whereHas('allergens', function($query) use ($allergenId) {
            $query->where('allergen_id', $allergenId);
        })->get();
    }

    /**
     * Obtiene todos los ingredientes que coincidan en nombre.
     */
    public function searchByName(string $name) {
        // TODO implementar
        return $this->model->whereLike('name', "%$name%");
    }

}
