<?php

namespace App\Repositories\Interfaces;

interface IngredientRepositoryInterface extends RepositoryInterface {

    /**
     * Obtiene todos los ingredientes vegetarianos.
     */
    public function vegetarian();

    /**
     * Obtiene todos los ingredientes veganos.
     */
    public function vegan();

    /**
     * Obtiene todos los ingredientes con un alérgeno determinado.
     */
    public function withAllergen(int $allergenId);

    /**
     * Obtiene todos los ingredientes que coincidan en nombre.
     */
    public function searchByName(string $name);

}
