<?php

namespace App\Repositories\Interfaces;

use App\Http\Requests\SearchRequest;

interface RecipeRepositoryInterface extends RepositoryInterface {

    /**
     * Obtiene todas las recetas vegetarianas.
     */
    public function vegetarian();

    /**
     * Obtiene todas las recetas veganas.
     */
    public function vegan();

    /**
     * Obtiene todas las recetas que no superen un tiempo de elaboración dado.
     */
    public function maxTime(int $minutes);

    /**
     * Obtiene todas las recetas que coincidan en nombre.
     */
    public function searchByName(string $name);

    /**
     * Obtiene todas las recetas que satisfagan los criterios de una búsqueda.
     */
    public function search(SearchRequest $search);

}
