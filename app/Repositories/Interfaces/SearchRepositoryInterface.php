<?php

namespace App\Repositories\Interfaces;

use App\Model\Search;

interface SearchRepositoryInterface extends RepositoryInterface {

    /**
     * Realiza una búsqueda de resultados de recetas basados en los parámetros de búsqueda ofrecidos.
     */
    function perform(Search $search);

}
