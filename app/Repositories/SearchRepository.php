<?php

namespace App\Repositories;

use App\Model\Search;
use App\Repositories\Interfaces\SearchRepositoryInterface;

use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Model\Recipe;

use Illuminate\Support\Str;

class SearchRepository implements SearchRepositoryInterface {


    protected $model;

    public function __construct(Search $search) {
        $this->model = $search;
    }

    public function all() {
        return $this->model->all();
    }

    public function create(array $data) {

        // TODO: Create search
        $search = new Search();



        return $search;
    }

    public function update(array $data, $id) {
        $search = $this->model
                ->findOrFail($id);
        $search->update($data);

        return $search;
    }

    /**
     * Elimina una búsqueda guardada
     */
    public function delete($id) {
        return $this->model->destroy($id);
    }

    public function find($id) {
        if (null == ($recipe = $this->model->find($id))) {
            throw new ModelNotFoundException("Recipe not found");
        }
        return $recipe;
    }

    /**
     * Obtiene todas las búsquedas con un título coincidente
     */
    public function searchByTitle(string $name) {
        return $this->model::whereLike('title', "'%{$name}%'");
    }

    /**
     * Devuelve recetas que satisfagan los requisitos de una búsqueda
     *
     * @param search Instancia de búsqueda. La búsqueda puede ser autogenerada o predefinida.
     */
    public function perform(Search $search) {
        $results = $this->model::all();

        if ($search->name) {
             $results = $results->reject(function ($rec) use ($search) {
                return !Str::contains(strtolower($rec->name), strtolower($search->name));
            });
        }

        if ($search->includedIngredients) {
            $results = $results->reject(function ($rec) use ($search) {
                $ingredients = $rec->ingredients()->pluck('ingredient_id')->toArray();

                foreach ($search->includedIngredients as $includedIngredient) {
                    if (!in_array($includedIngredient, $ingredients)) {
                        return true;
                    }
                }
            });
        }

        if ($search->excludedIngredients) {
            $results = $results->reject(function ($rec) use ($search) {
                $ingredients = $rec->ingredients()->pluck('ingredient_id')->toArray();

                foreach ($search->excludedIngredients as $excludedIngredient) {
                    if (in_array($excludedIngredient, $ingredients)) {
                        return true;
                    }
                }
            });
        }

        if ($search->maxtime) {
            $results = $results->reject(function (Recipe $recipe) use ($search) {
                return $recipe->time > $search->maxtime;
            });
        }

        if ($search->isvegetarian) {
            $results = $results->filter(function(Recipe $recipe) {
                return $recipe->isVegetarian();
            });
        }

        if ($search->isvegan) {
            $results = $results->filter(function(Recipe $recipe) {
                return $recipe->isVegan();
            });
        }

        if ($search->allergens) {
            $results = $results->filter(function(Recipe $recipe) use ($search) {
                $allergens = $recipe->ingredients()->pluck('allergen_id')->toArray();

                foreach ($search->allergens as $allergen) {
                    if (!in_array($allergen, $allergens)) {
                        return true;
                    }
                }
            });
        }

        if ($search->includedTags) {
            foreach ($search->includedTags as $tag) {
                $results = $results->filter(function (Recipe $recipe) use ($tag) {
                    $tags = $recipe->tags()->pluck('tagname')->toArray();
                    return in_array($tag, $tags, true);
                });
            }
        }

        return $results;

    }

}
