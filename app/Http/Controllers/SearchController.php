<?php

namespace App\Http\Controllers;

use \Exception;

use App\Repositories\Interfaces\SearchRepositoryInterface;

use App\Http\Requests\SearchRequest;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Helpers\JwtAuth;

class SearchController extends Controller
{

    /**
     * @var SearchRepositoryInterface
     */
    private $repository;

    public function __construct(SearchRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->repository->all();
    }

    /**
     * Gets the current logged-in-user searches.
     */
    public function userSearches(Request $request)
    {
        $hash = $request->header('Authorization', null);

        // El usuario se obtiene a partir del token
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if ($checkToken) {
            echo "Authenticated";
        } else {
            echo "NOT authenticated";
        }

        die();

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\SearchRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SearchRequest $request)
    {
        try {
            $search = $this->repository->create($request->toArray());
            return response()->json([
                'success' => true,
                'search' => $search
            ], 200);

        } catch (Exception $exception) {
            return response()->json([
                'success' => false,
                'error' => $exception->getMessage(),
            ], 401);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return $this->repository->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param SearchRequest  $request
     * @param int  $id
     * @return Response
     */
    public function update(SearchRequest $request, $id)
    {
        try {
            $search = $this->repository->update($request->toArray(), $id);

            return response()->json([
                "success" => true,
                "search" => $search
            ], 200);

        } catch (Exception $exception) {
            return response()->json([
                "success" => false,
                "error" => 'Error updating search data'
            ], 401);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->repository->delete($id);
    }


    /**
     * Realiza la búsqueda almacenada con el ID correspondiente.
     *
     * @param $id int El identificador de la búsqueda almacenada.
     * @return \Illuminate\Http\Response
     */
    public function perform($id) {

        try {
            // Intentamos obtener la búsqueda almacenada.
            $search = $this->repository->find($id);
            // Aplicamos la búsqueda almacenada.
            $search->includedIngredients = $search->includedIngredients()->pluck('id')->toArray();
            $search->excludedIngredients = $search->excludedIngredients()->pluck('id')->toArray();
            $search->allergens = $search->allergens()->pluck('name')->toArray();

            $results = $this->repository->perform($search);
            // Si todo ha ido bien, obtendremos resultados.
            return response()->json([
                "success" => true,
                "status" => 200,
                "message" => "Búsqueda realizada con éxito",
                "search" => $search,
                "recipes" => $results

            ], 200);


        } catch (Exception $ex) {
            return $ex;
            return response()->json([
                "success" => false,
                "status" => 400,
                "message" => "Error al cargar búsqueda",
            ], 400);
        }


    }

}
