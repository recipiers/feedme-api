<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Model\Allergen;
use App\Http\Requests\AllergenRequest;

class AllergenController extends Controller
{
      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Allergen::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\AllergenRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AllergenRequest $request)
    {
        Allergen::create($request->all());
        return response()->json(
            [
                "success" => true
            ],
            200
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Allergen::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AllergenRequest $request, $id)
    {
        $allergen = Allergen::findOrFail($id);
        return $allergen->update($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Allergen::findOrFail($id)->delete();
    }
}
