<?php

namespace App\Http\Controllers;

use App\Model\MeasureUnit;

use App\Http\Requests\MeasureUnitRequest;

class MeasureUnitController extends Controller
{
    /*
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function index()
   {
       return MeasureUnit::all();
   }

   /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
   public function store(MeasureUnitRequest $request)
   {
       MeasureUnit::create($request->all());
       return response()->json([
            "success" => true
        ], 200);
   }

   /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function show($id)
   {
       return MeasureUnit::findOrFail($id);
   }

   /**
    * Update the specified resource in storage.
    *
    * @param MeasureUnitRequest  $request
    * @param int  $id
    * @return \Illuminate\Http\Response
    */
   public function update(MeasureUnitRequest $request, $id)
   {
       return MeasureUnit::update($request->all());
   }

   /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function destroy($id)
   {
       return MeasureUnit::findOrFail($id)->delete();
   }
}
