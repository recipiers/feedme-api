<?php

namespace App\Http\Controllers;

use App\Helpers\JwtAuth;
use App\Model\Recipe;
use Illuminate\Http\Request;

use App\Model\User;
use App\Model\Search;

use App\Http\Requests\ChangePasswordRequest;

use App\Http\Requests\PrimarySearchRequest;
use App\Http\Requests\StoredSearchRequest;

class UserController extends Controller
{

    public function getCurrentUser(Request $request) {

        $user = $this->getUserByTokenHeader($request);
        return response()->json([
            "success" => true,
            "status" => 200,
            "message" => "OK",
            "user" => $user,
            "fav_recipes" => $user->favouriteRecipes()->get(),
            "searches" => "",
        ]);

    }

    public function getCurrentUserFavouriteRecipes(Request $request) {

        $user = $this->getUserByTokenHeader($request);
        $favRecipes = $user->favouriteRecipes()->get();

        return response()->json([
            "success" => true,
            "status" => 200,
            "message" => "OK",
            "fav_recipes" => $favRecipes,
        ]);
    }

    public function savedSearches(Request $request) {
        $user = $this->getUserByTokenHeader($request);
        return response()->json([
            'searches' => $user->searches()->get(),
            'status' => 200,
            'message' => 'Here are your saved searches'
        ]);
    }

    public function changePassword(ChangePasswordRequest $request) {
        $user = $this->getUserByTokenHeader($request);
        if (hash('sha256', $request->oldpassword) == $user->password) {
            $user->password = hash('sha256', $request->newpassword);
            $user->save();
            return response()->json([
                'success' => true,
                'message' => 'Contraseña cambiada con éxito'
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Ha fallado el cambio de contraseña: La contraseña antigua no es correcta'
            ], 400);
        }

        $user->save();
    }



    public function deleteCurrentUser(Request $request) {

        $user = $this->getUserByTokenHeader($request);
        $user->delete();
        return response()->json([
            'success' => true,
            'status' => 200,
            'message' => "Cuenta de usuario eliminada",
            "user" => $user
        ]);

    }

    /**
     * Obtiene el usuario a partir de la token de autenticación de la petición.
     */
    private function getUserByTokenHeader(Request $request ) {

        $jwtAuth = new JwtAuth();
        $hash = $request->header('Authorization', null);

        $checkToken = $jwtAuth->checkToken($hash, true);

        if ($checkToken) {
            $userId = $checkToken->sub;
            $user = User::find($userId);
            return $user;
        } else {
            // ERROR DE AUTENTICACIÓN
            return response()->json([
                "success" => false,
                "status" => 403,
                "message" => "Authorization Token error"
            ]);
        }

    }

    public function toggleRecipeAsFavourite(Request $request, $id) {

        $user = $this->getUserByTokenHeader($request);
        $recipe = Recipe::find($id);

        if ($user->favouriteRecipes()->where('recipe_id','=',$id)->get()->isEmpty()) {
            $user->favouriteRecipes()->attach(['recipe_id'=>$id]);
            return response()->json([
                "success" => true,
                "message" => "Receta marcada como favorita",
                "recipe" => $recipe
            ], 200);
        } else {
            $user->favouriteRecipes()->detach(['recipe_id' => $id] );

            return response()->json([
                "success" => true,
                "message" => "Receta desmarcada de favoritas",
                "recipe" => $recipe
            ], 200);
        }
    }

    public function getPrimarySearch(Request $request) {

        $user = $this->getUserByTokenHeader($request);

        $primarySearch = Search::where([
            "user_id" => $user->id,
            "isprimarysearch" => true
        ])->first();

        $primarySearch->includedIngredients = $primarySearch->includedIngredients()->get()->toArray();
        $primarySearch->excludedIngredients = $primarySearch->excludedIngredients()->get()->toArray();
        $primarySearch->allergens = $primarySearch->allergens()->get()->toArray();


        return [
            "success" => true,
            "primarySearch" => $primarySearch
        ];

    }
    // This do not include the primary search
    public function getStoredSearches(Request $request) {
        $user = $this->getUserByTokenHeader($request);

        $storedSearches = Search::where([
            "user_id" => $user->id,
            "isprimarysearch" => false
        ])->get();

        return [
            "success" => true,
            "storedSearches" => $storedSearches
        ];
    }

    public function storeNewSearch(StoredSearchRequest $request) {
        $user = $this->getUserByTokenHeader($request);
        if ( $user->searches()->count() < 4) {
            $search = $user->searches()->create($request->toArray());

            if ($request->includedIngredients) {
                foreach( $request->includedIngredients as $included) {
                    $search->ingredients()->attach(
                        $included
                    );
                }
            }

            if ($request->excludedIngredients) {
                foreach ($request->excludedIngredients as $excluded) {
                    $search->ingredients()->attach(
                        $excluded,
                        ['isincluded' => false]
                    );
                }
            }

            if ($request->allergens) {
                foreach ($request->allergens as $allergen) {
                    $search->allergens()->attach($allergen);
                }
            }
            $search->save();
            return [
                "success" => true,
                "message" => "Búsqueda almacenada con éxito"
            ];
        } else {
            return [
                "success" => false,
                "message" => "Ya tienes el máximos de busquedas salvas, borre o edite una dellas"
            ];
        }
    }

    public function updateStoredSearch(StoredSearchRequest $request, $id) {
        $user = $this->getUserByTokenHeader($request);

        $search = Search::where([
            'user_id' => $user->id,
            'id' => $id,
            'isprimarysearch' => false
        ])->first();

        if ($search) {

            $search->title = $request->title ?? '';
            $search->name = $request->name ?? '';
            $search->isvegan = $request->isvegan ?? false;
            $search->isvegetarian = $request->isvegetarian ?? false;
            $search->maxtime = $request->maxtime ?? 0;

            $search->ingredients()->detach();

            if ($request->includedIngredients) {

                foreach ($request->includedIngredients as $included) {
                    $search->ingredients()->attach(
                        $included,
                        ['isincluded' => true]
                    );
                }
            }

            if ($request->excludedIngredients) {

                foreach ($request->excludedIngredients as $excluded) {
                    $search->ingredients()->attach(
                        $excluded,
                        ['isincluded' => false]
                    );
                }
            }

            $search->allergens()->detach();

            if ($request->allergens) {

                foreach ($request->allergens as $allergen) {
                    $search->allergens()->attach($allergen);
                }
            }

            if ($search->save()) {
                return response()->json([
                    'success' => true,
                    'message' => 'Búsqueda actualizada con éxito'
                ]);
            } else {
                return response()->json([
                    'success' => false,
                    "message" => "No se han podido actualizar los criterios de búsqueda."
                ], 400);

            }
        } else {
            return response()->json([
                'success' => false,
                "message" => "La búsqueda no existe"
            ], 404);
        }
    }

    public function getStoredSearch(StoredSearchRequest $request, $id) {
        $user = $this->getUserByTokenHeader($request);

        $search = Search::where([
            'user_id' => $user->id,
            'id' => $id,
            'isprimarysearch' => false
        ])->first();
            // App\Model\User::find(14)->searches()->find(8)->ingredients()->get()
        if ($search) {

            $search->includedIngredients = $search->includedIngredients()->get()->toArray();
            $search->excludedIngredients = $search->excludedIngredients()->get()->toArray();
            $search->allergens = $search->allergens()->get()->toArray();

            return response()->json([
                'success' => true,
                'message' => 'OK',
                'search' => $search
            ]);

        } else {
            return response()->json([
                'success' => false,
                "message" => "La búsqueda no existe",
                'search' => null
            ], 404);
        }
    }

    public function deleteStoredSearch(StoredSearchRequest $request, $id) {
        $user = $this->getUserByTokenHeader($request);

        $search = Search::where([
            'user_id' => $user->id,
            'id' => $id,
            'isprimarysearch' => false
        ])->first();

        if ($search) {

            $search->delete();

            return response()->json([
                'success' => true,
                'message' => 'Búsqueda borrada con éxito',
                'search' => $search
            ]);

        } else {
            return response()->json([
                'success' => false,
                "message" => "La búsqueda no existe",
                'search' => null
            ], 404);
        }
    }


    public function updatePrimarySearch(PrimarySearchRequest $request) {
        $user = $this->getUserByTokenHeader($request);

        $search = Search::where([
            'user_id' => $user->id,
            'isprimarysearch' => true
        ])->first();

        if ($search) {

            $search->title = $request->title ?? '';
            $search->name = $request->name ?? '';
            $search->isvegan = $request->isvegan ?? false;
            $search->isvegetarian = $request->isvegetarian ?? false;

            $search->ingredients()->detach();

            if ($request->excludedIngredients) {
                foreach ($request->excludedIngredients as $excluded) {
                    $search->ingredients()->attach(
                        $excluded,
                        ['isincluded' => false]
                    );
                }
            }

            if ($request->includedIngredients) {
                foreach ($request->includedIngredients as $included) {
                    $search->ingredients()->attach(
                        $included,
                        ['isincluded' => true]
                    );
                }
            }

            $search->allergens()->detach();


            if ($request->allergens) {
                foreach ($request->allergens as $allergen) {
                    // De verdad que aceptamos tanto el id de la alergia cuanto su nombre
                    // En la documentacion ponemos la manera preferible pero si alguien no le la documentacion
                    // Tambien quiero que funcione
                    if (is_numeric($allergen)) {
                        $search->allergens()->attach($allergen);
                    } else {
                        $search->allergens()->attach(\App\Model\Allergen::where('name','like','%'.$allergen.'%')->first()->id);
                    }
                }
            }

            if ($search->save()) {
                return response()->json([
                    'success' => true,
                    'message' => 'Búsqueda primaria actualizada con éxito',
                    'search' => $search
                ]);
            } else {
                return response()->json([
                    'success' => false,
                    "message" => "No se han podido actualizar los criterios de búsqueda.",
                    'search' => null
                ], 400);

            }
        } else {
            return response()->json([
                'success' => false,
                "message" => "No se ha podido encontrar la búsqueda primaria de tu usuario... bórrate la cuenta XD"
            ], 404);
        }
    }


}
