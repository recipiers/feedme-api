<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\JwtAuth;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Model\User;

use App\Http\Requests\RegisterRequest;

class RegisterController extends Controller
{

    use RegistersUsers;

    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /* Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    /**
     * DECLARADO EN EL TRAIT 'RegistersUser'.
     */
    public function register(RegisterRequest $request)
    {

        $user = new User($request->toArray());
        $user->isAdmin = false;
        $user->password = hash('sha256', $user->password);

        $userExists = User::where('email', '=', $user->email)->first() !== null;

        if ($userExists) {
            // USUARIO EXISTE. NO SUSCRIBIR
            return response()->json([
                "success" => false,
                "error" => "Ya existe un usuario con email='{$request->email}'."
                ]);
        } else {
            $user->save();

            $user->searches()->create([
                'isprimarysearch' => true,
                'title' => ''
            ]);

            return response()->json([
                "success" => true,
                "message" => "Registro OK",
                "user" => User::where('email', '=', $user->email)->first()
            ]);

        }

    }

    protected function registered(Request $request, User $user) {
        $jwtAuth = new JwtAuth();

        $jwtAuth->signup($user->email, hash('sha256', $user->password));
        // Registro exitoso, pasa los datos de usuario a la respuesta
        return response()->json([
            'success' => true,
            'status' => 201,
            'user' => $user->toArray()
        ], 201);
    }



}
