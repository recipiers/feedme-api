<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests\LoginRequest;
use App\Helpers\JwtAuth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(LoginRequest $request)
    {
        $jwtAuth = new JwtAuth();
        // Obtener los datos para crear la token
        $email = $request->email;
        $getToken = $request->getToken; // Cambiar si hace falta por alguna comprobación
        $hashPassword = hash('sha256', $request->password);
        // Efectuar autenticación con JWT

        $success = false;
        $message = 'OK';
        $status = 200;
        $token = null;
        $user = null;

        if (!is_null($email) && !is_null($request->password)) {
            $token = $jwtAuth->signup($email, $hashPassword, $getToken);
            $user = $jwtAuth->signup($email, $hashPassword);

            if ($token && $user) {
                // Login OK
                $success = true;
                $message = "Login OK";

            } else {
                // Credenciales no válidas
                $success = false;
                $message = "El email y/o la contraseña no son correctos";
                $status = 401;
            }

        } else {
            $status = 500;
            $message = "Datos de entrada no válidos";
        }

        return response()->json([
            "success" => $success,
            "message" => $message,
            "status" => $status,
            "user" => $user,
            "token" => $token
        ]);

    }

    public function logout()
    {
        $user = Auth::guard('api')->user();

        if ($user) {
            $user->api_token = null;
            $user->save();
        }

        return response()->json([
            'data' => 'User logged out!'
        ]);

    }

}
