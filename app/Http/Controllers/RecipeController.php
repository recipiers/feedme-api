<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchRequest;
use App\Http\Requests\StoreRecipeInfoRequest;
use App\Model\Recipe;
use App\Repositories\Interfaces\RecipeRepositoryInterface;
use Illuminate\Http\Request;
use \Exception;

class RecipeController extends Controller
{

    /**
     * @var RecipeRepositoryInterface
     */
    private $repository;

    public function __construct(RecipeRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->repository->all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreRecipeInfoRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRecipeInfoRequest $request)
    {
        try {
            $recipe = $this->repository->create($request->toArray());
            return response()->json([
                'success' => true,
                'recipe' => $recipe,
            ], 200);

        } catch (Exception $exception) {
            return response()->json([
                'success' => false,
                'error' => $exception->getMessage(),
            ], 401);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $recipe = $this->repository->find($id);

            return response()->json([
                "success" => true,
                "status" => 200,
                "recipe" => $recipe,
            ], 200);

        } catch (Exception $e) {

            return response()->json([
                "success" => false,
                "status" => 404,
                "recipe" => null,
            ], 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $recipe = $this->repository->update($request->toArray(), $id);

            return response()->json([
                "success" => true,
                "status" => 200,
                "recipe" => $recipe,
                "message" => "Receta modificada con éxito",
            ], 200);

        } catch (Exception $exception) {
            return response()->json([
                "success" => false,
                "status" => 401,
                "message" => 'No se ha podido actualizar la receta',
            ], 401);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $recipe = $this->repository->find($id);
            return response()->json([
                'success' => true,
                'message' => 'Recepta borrada con éxito',
                'recipe' => $recipe,
            ]);
        } catch (Exception $e) {
            return response()->json([
                'success' => false,
                'message' => 'Recepta no borrada (fallo)',
                'recipe' => $recipe,
            ]);
        }
    }

    /**
     * Obtiene todas las recetas que cumplan los criterios de búsqueda dados.
     */
    public function search(SearchRequest $request)
    {

        $recipes = $this->repository->search($request);

        return [
            'success' => true,
            'status' => 200,
            'search' => $request->toArray(),
            'recipes' => $recipes,
        ];

    }
}
