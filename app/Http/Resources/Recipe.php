<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Recipe extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'steps' => $this->steps()->orderBy('ordernum')->get('description')->toArray(),
            'ingredients'=> $this->ingredients_measure(),
            'description' => $this->description,
            'time' => $this->time,
            'pictureurl' => $this->pictureurl,
            'numportions' => $this->numportions,
            'difficulty' => $this->difficulty
        ];
    }
}
