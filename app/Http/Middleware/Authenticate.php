<?php

namespace App\Http\Middleware;

use App\Model\User;
use App\Helpers\JwtAuth;
use Closure;

class Authenticate
{
    /**
     * Middleware de autenticación. Se ejecuta antes de entrar en cualquier ruta que requiera autenticación.
     */
    public function handle($request, Closure $next, ...$guards) {

        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken(
                $request->header('Authorization', null), true);

        // TODO: Comprobar expiración de la token

        // Si falla la verificación de la token o el usuario al que corresponde no existe, se enviará un error de autorización.
        if (!$checkToken || !User::find($checkToken->sub)) {
            return response()->json([
                "success" => false,
                "status" => 403,
                "message" => "Authorization Token error"
            ]);
        }

        return $next($request);
    }


}
