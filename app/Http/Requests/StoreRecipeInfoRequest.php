<?php

/**
 * Esta clase tiene como finalidad validar los datos que llegan a través de una request cuando
 * ésta tiene como objetivo crear o modificar un recurso de tipo receta.
 */



namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreRecipeInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Cambiar por método de comprobación de permisos.
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
                'min:4',
                'max:128'
            ],
            'description' => [
                'required',
                'min: 4',
                'max:255'
            ],
            'time' => [
                'required',
                'integer',
                'min: 3',
                'max: 1200'
            ],
            'pictureurl' => [
                'required',
                'ends_with:.jpeg,.png,.jpg,.gif,.svg',
                'max:2048'
            ],
            'numportions' => [
                'numeric',
                'min:1',
                'max:10000'
            ],
            'difficulty' => [
                'integer',
                'min:1',
                'max:5'
            ],
            'steps' => [
                'array'
            ],
            'steps.*' => [
                'required'
            ],
            'ingredients' => [
                'array'
            ],
            'ingredients.*' => [
                'required'
            ]
        ];
    }
}
