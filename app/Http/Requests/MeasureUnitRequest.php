<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MeasureUnitRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'unique', 'min: 2', 'max:255'],
            'genericequivalent' => ['required', 'default:1.0'],
            'symbol' => ['required', 'string', 'unique', 'min:2','max:255'],
            'measureunittype_id' => ['required', 'exists:measureunittype,id']
        ];
    }
}
