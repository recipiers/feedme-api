<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoredSearchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => [
                'min:1',
                'max:127'
            ],
            'name' => [
                'min:1',
                'max:127'
            ],
            'isvegetarian' => [
                'boolean',
            ],
            'isvegan' => [
                'boolean',
            ],
            'maxtime' => [
                'integer',
                'min: 0',
                'max: 999'
            ],
            'includedIngredients' => [
                'array'
            ],
            'excludedIngredients' => [
                'array'
            ],
            'includedTags' => [
                'array'
            ],
            'allergens' => [
                'array'
            ],
        ];
    }
}
