<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MeasureUnitType extends Model
{
    public $timestamps = false;
    public $table = 'measureunittypes';

    protected $fillable = [
        'name'
    ];

    public function genericUnit() {
        return $this->hasOne('App\Model\MeasureUnit');
    }

    public function measureUnits() {
        return $this->hasMany('App\Model\MeasureUnit');
    }
}
