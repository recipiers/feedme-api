<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Step extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'ordernum', // Unique for a given recipe
        'description',
        'recipe_id'
    ];

   /**
     * Get the recipe that owns the step.
     */
    public function recipe()
    {
        return $this->belongsTo('App\Model\Recipe', 'recipe_id');
    }

}
