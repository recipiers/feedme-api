<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Relations\Pivot;

class RecipeIngredientMeasure extends Pivot
{
    public $timestamps = false;
    protected $primaryKey = ['recipe_id','ingredient_id'];
    protected $table = "recipe_ingredient_measureunit";
    protected $autoincrement = false;
    protected $fillable = [
        'recipe_id',
        'ingredient_id',
        'measureunit_id',
        'amount'
    ];

    public function recipe() {
        return $this->belongsTo('App\Model\Recipe', 'recipe_id');
    }

    public function ingredient() {
        return $this->belongsTo('App\Model\Ingredient', 'ingredient_id');
    }

    public function measureunit() {
        return $this->belongsTo('App\Model\MeasureUnit', 'measureunit_id');
    }
}
