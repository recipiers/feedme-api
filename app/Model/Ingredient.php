<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'name',
        'description',
        'isvegan',
        'isvegetarian'
    ];

    public function measureTypes() {
        return $this->hasMany('App\Model\MeasureType');
    }

    public function allergens() {
        return $this->belongsToMany('App\Model\Allergen');
    }

    public function recipes(){
        return $this->belongsToMany('App\Model\RecipeIngredientMeasure');
    }

}
