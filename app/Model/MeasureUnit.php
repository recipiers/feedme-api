<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MeasureUnit extends Model
{
    public $timestamps = false;
    public $table = 'measureunits';

    protected $fillable = [
        'name',
        'genericequivalent', // Number of units that equals to the generic equivalent for its type. 1 for generic units
        'symbol'
    ];

    public function type()
    {
        return $this->belongsTo('App\Model\MeasureUnitType', 'measureunittype_id');
    }

    function recipeIngredientMeasure()
    {
        return $this->hasMany('App\Model\RecipeIngredientMeasure', 'measureunit_id');
    }

    /*
    public function isGenericFrom()
    {
        return $this->belongsTo('App\Model\MeasureUnitType', 'generictype_id');
    }
    */
}
