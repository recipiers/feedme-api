<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Search extends Model
{
    public $timestamps = false;
    protected $table = 'user_searches';
    protected $fillable = [
        'title',
        'name',
        'isvegetarian',
        'isvegan',
        'maxtime',
        'isprimarysearch'
    ];

    protected $attributes = [
        'isprimarysearch' => false,
    ];

    protected $hidden = [
        'user_id',
        'isprimarysearch'
    ];

    public function ingredients() {
        return $this->belongsToMany('App\Model\Ingredient');
    }

    public function includedIngredients() {
        return $this->ingredients()->where('isincluded','=','1');
    }

    public function excludedIngredients() {
        return $this->ingredients()->where('isincluded','=','0');
    }

    public function tags() {
        return $this->belongsToMany('App\Model\Tag');
    }

    public function allergens() {
        return $this->belongsToMany('App\Model\Allergen', 'search_allergens');
    }

    public function user() {
        return $this->belongsTo('App\Model\User');
    }

}
