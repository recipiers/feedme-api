<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    public $timestamps = false;
    protected $table = 'recipe_tags';
    protected $fillable = ['recipe_id','tagname'];
    public $incrementing = false;
    protected $tableName = 'recipe_tags';
    protected $primaryKey = ['recipe_id','tagname'];
    /**
     * Get all of the recipes that are assigned this tag.
     */
    public function recipes()
    {
        return $this->belongsTo('App\Model\Recipe', 'recipe_id')->getResults();
    }

    /**
     * Get all of the searches that are assigned this tag.
     */
    public function searches()
    {
        return $this->belongsToMany('App\Model\Search','search_tag','tagname');
    }
}
