<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Allergen extends Model
{

    protected $table = 'allergens';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'description'
    ];

    public function ingredient() {
        return $this->belongsToMany('App\Model\Ingredient');
    }

    public function search() {
        return $this->belongsToMany('App\Model\Search');
    }
}
