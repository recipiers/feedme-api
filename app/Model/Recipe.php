<?php

namespace App\Model;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use App\Traits\SQLMethods;

use App\Model\Ingredient;

class Recipe extends Model
{
    use SQLMethods;
    protected $table = 'recipes';
    public $timestamps = false;

    protected $fillable = [
        'name',
        'description',
        'time', // In minutes
        'pictureurl',
        'numportions', // 1..n
        'difficulty' // 1..5
    ];

    public function ingredients()
    {
        return $this->hasMany('App\Model\RecipeIngredientMeasure');
    }

    /**
     * Get all of the tags for the recipe.
     */
    public function tags()
    {
        return $this->hasMany('App\Model\Tag');
    }

    /**
     * Get all steps for the recipe.
     */
    public function steps()
    {
        return $this->hasMany('App\Model\Step');
    }

    public function isUserFavourite() {
        return $this->belongsToMany('App\Model\User');
    }

    public function isVegetarian() {
        // Si un ingrediente no es vegano ni vegetariano, la receta se considera no vegana
        foreach ($this->ingredients()->get() as $recipeIngredientMeasure) {
            $ingredient = Ingredient::find($recipeIngredientMeasure->ingredient_id);

            if (!$ingredient->isvegan && !$ingredient->isvegetarian) {
                return false;
            }
        }
        return true;
    }

    public function isVegan() {
        // Si un ingrediente no es vegano, la receta se considera no vegana
        foreach ($this->ingredients()->get() as $recipeIngredientMeasure) {
            $ingredient = Ingredient::find($recipeIngredientMeasure->ingredient_id);

            if (!$ingredient->isvegan) {
                return false;
            }
        }
        return true;
    }

}
