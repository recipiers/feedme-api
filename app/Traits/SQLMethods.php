<?php

namespace App\Traits;
use Illuminate\Support\Facades\DB;

trait SQLMethods
{

    //
    // Dear maintainer:
    //
    // Once you are done trying to 'optimize' this routine,
    // and have realized what a terrible mistake that was,
    // please increment the following counter as a warning
    // to the next guy:
    //
    // total_hours_wasted_here = 48
    //

    // get list of ingredients and quantities for this recipe
    public function ingredients_measure()
    {
        return DB::table('recipe_ingredient_measureunit')
            ->join('ingredients', 'recipe_ingredient_measureunit.ingredient_id', '=', 'ingredients.id')
            ->join('measureunits', 'measureunits.id', 'recipe_ingredient_measureunit.measureunit_id')
            ->select('recipe_ingredient_measureunit.amount', 'measureunits.symbol', 'ingredients.name','measureunits.id as measureunit_id','ingredients.id as ingredient_id')
            ->where('recipe_ingredient_measureunit.recipe_id', '=', $this->id)
            ->get()
            ->toArray();
    }

    // add another ingredient with measure to this recipe
    public function addIngredientMeasure($ingredientid, $measureunitid, $amount)
    {
        //  SQL JUST WORKS
        return DB::insert(
            'insert into recipe_ingredient_measureunit (recipe_id,ingredient_id,measureunit_id,amount) values (?,?,?, ?)',
            [$this->id, $ingredientid, $measureunitid, $amount]
        );
    }

    // delete ingredient from recipe
    public function delIngredient($ingredientid)
    {
        //  SQL JUST WORKS
        return DB::insert(
            'delete from recipe_ingredient_measureunit where (recipe_id,ingredient_id) = (?,?)',
            [$this->id, $ingredientid]
        );
    }
}
