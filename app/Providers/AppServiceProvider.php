<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\Builder;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Registramos el repositorio de recetas y su interfaz
        // De esta forma, podremos inyectarlo al controlador
        $this->app->bind(
            'App\Repositories\Interfaces\RecipeRepositoryInterface',
            'App\Repositories\RecipeRepository'
        );

        $this->app->bind(
            'App\Repositories\Interfaces\SearchRepositoryInterface',
            'App\Repositories\SearchRepository'
        );

        $this->app->bind(
            'App\Repositories\Interfaces\IngredientRepositoryInterface',
            'App\Repositories\IngredientRepository'
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Schema::defaultStringLength(191);

/*
                       ,---.
                       /    |
                      /     |
                     /      |
                    /       |
               ___,'        |
             <  -'          :         W T F ? !
              `-.__..--'``-,_\_
                 |o/ ` :,.)_`>        I T ' S   F * C K I N G   M A G I C ? !
                 :/ `     ||/)
                 (_.).__,-` |\                         #
                 /( `.``   `| :                        #
                 \'`-.)  `  ; ;                        #
*                | `       /-<                         #
·~               |     `  /   `.                       #
 ,-_-..____     /|  `    :__..-'\                    #####
/,'-.__\\  ``-./ :`      ;       \                    ###
`\~/\  `\\  \ :  (   `  /  ,   `. \                    #
  \` \   \\   |  | `   :  :     .\ \
   \ `\_  ))  :  ;     |  |      ): :
  (`-.-'\ ||  |\ \   ` ;  ;       | |
   \-_   `;;._   ( `  /  /_       | |
    `-.-.// ,'`-._\__/_,'         ; |
     ( \:: :     /     `     ,   /  |
      \_|| |    (        ,' /   /   |
        ||                ,'   / SSt|

*/

        // Creamos una macro para hacer búsquedas en múltiples campos. Será útil para establecer búsquedas
        Builder::macro('whereLike', function($attributes, $terms) {

            $this->where(function($query) use ($attributes, $terms) {
                foreach (array_wrap($attributes) as $attribute) {
                    foreach (array_wrap($terms) as $term) {
		    
			#$query->orWhereRaw("UPPER(`$attribute`) LIKE UPPER($term)");

                        $query->orWhere(strtoupper($attribute),'LIKE', strtoupper($term) );
		    }
                }
            });

            return $this;
        });
    }


}
